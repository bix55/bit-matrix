all: kmer #kmer35 #ptx
SRC = main.cu kmer.cu crc.cu radixsort.cu reduction.cu

kmer: $(SRC)
	/usr/local/cuda-8.0/bin/nvcc -arch=compute_61 -code=sm_61 -maxrregcount=32 -Xcompiler "-Wall -O3" -lineinfo -rdc=true -o kmer $(SRC)

ptx: $(SRC)
	nvcc -arch=compute_61 -code=sm_61 -Xcompiler "-Wall -O3" -lineinfo -rdc=true -ptx -o ptx $(SRC)

kmer35: $(SRC)
	/usr/local/cuda-7.5/bin/nvcc -arch=compute_35 -code=sm_35 -Xcompiler "-Wall -O3" -Xptxas -v -lineinfo -rdc=true -o kmer35 $(SRC)

aln: aln.cu
	/usr/local/cuda-8.0/bin/nvcc -arch=compute_61 -code=sm_61 -maxrregcount=32 -Xcompiler "-Wall -O3" -lineinfo -rdc=true -o aln aln.cu

.PHONY: clean
clean:
	rm -f kmer kmer35 ptx *~ .*~
