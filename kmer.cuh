#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <time.h>
#include "simd_functions.h"

#define LOP3(A, B, C, D, E) asm("lop3.b32 %0, %1, %2, %3, "#E";" : "=r"(D) : "r"(A), "r"(B), "r"(C))

const uint32_t UNIFIED = 0;

const uint32_t DEBUG = 0;
const uint32_t PRINT = 1;

const uint32_t BLOCKS  = 40;
const uint32_t THREADS = 1024;

// const uint32_t K = 15;          // k-mer
const uint32_t K = 14;          // k-mer
// const uint32_t K = 13;          // k-mer
// const uint32_t K = 12;          // k-mer
//const uint32_t K = 11;          // k-mer

const uint32_t M    = 8192;     // Bloom Filter size(bit)
const uint32_t MCNT = 256;      // Bloom Filter count          [M/32]
const uint32_t MBIT = 8;        // Bloom Filter count bitwidth [log(MCNT)]
const uint32_t H    = 5;        // Number of Hash function
const uint32_t HBIT = 13;       // Hash value bitwidth         [log(M)]

const uint32_t MASK   = 0x1FFF; // Bloom Filter size mask      [M-1]
const uint32_t CNTIDX = 8;      // Count Index                 [M/THREADS]
const uint32_t CNTSFT = 2;      // Count Shift                 [log(THREADS/MCNT)]
const uint32_t CNTMSK = 3;      // Count Mask                  [32/CNTIDX-1]
const uint32_t HCNT   = 4;      // Hit count                   [THREADS/MCNT]

// const uint32_t KH = 1024;
// const uint32_t POW = 4294967296; // 4 ^ 16
// const uint32_t KH  = 256;
// const uint32_t POW = 1073741824; // 4 ^ 15

const uint32_t KH  = 64;
const uint32_t POW = 268435456; // 4 ^ 14

// const uint32_t KH  = 16;
// const uint32_t POW = 67108864;  //4 ^ 13
// const uint32_t KH  = 4;
// const uint32_t POW = 16777216;  //4 ^ 12

// const uint32_t KH  = 1;
// const uint32_t POW = 4194304;   //4 ^ 11

/*
const uint32_t K = 12;          // k-mer

const uint32_t M    = 4096;     // Bloom Filter size(bit)
const uint32_t MCNT = 128;      // Bloom Filter count          [M/32]
const uint32_t MBIT = 7;        // Bloom Filter count bitwidth [log(MCNT)]
const uint32_t H    = 6;        // Number of Hash function
const uint32_t HBIT = 12;       // Hash value bitwidth         [log(M)]

const uint32_t MASK   = 0xFFF;  // Bloom Filter size mask      [M-1]
const uint32_t CNTIDX = 4;      // Count Index                 [M/THREADS]
const uint32_t CNTSFT = 3;      // Count Shift                 [log(THREADS/MCNT)]
const uint32_t CNTMSK = 7;      // Count Mask                  [32/CNTIDX-1]
const uint32_t HCNT   = 8;      // Hit count                   [THREADS/MCNT]

const uint32_t KH  = 2;
const uint32_t POW = 16777216;  //4 ^ 12
*/

/*
const uint32_t K = 11;          // k-mer

const uint32_t M    = 32768;    // Bloom Filter size(bit)
const uint32_t MCNT = 1024;     // Bloom Filter count          [M/32]
const uint32_t MBIT = 10;       // Bloom Filter count bitwidth [log(MCNT)]
const uint32_t H    = 6;        // Number of Hash function
const uint32_t HBIT = 15;       // Hash value bitwidth         [log(M)]

const uint32_t MASK   = 0x7FFF; // Bloom Filter size mask      [M-1]
const uint32_t CNTIDX = 32;     // Count Index                 [M/THREADS]
const uint32_t CNTSFT = 0;      // Count Shift                 [log(THREADS/MCNT)]
const uint32_t CNTMSK = 0;      // Count Mask                  [32/CNTIDX-1]
const uint32_t HCNT   = 1;      // Hit count                   [THREADS/MCNT]

const uint32_t KH  = 4;
const uint32_t POW = 4194304;   //4 ^ 11
*/

const uint64_t MAX_READ_LEN = 55000;
const uint64_t MAX_READ_NUM = 1000000;

const uint32_t MHENABLE  = 0;
const uint32_t MH        = 6;
const uint32_t MINIMIZER = 1;
const uint32_t W         = 9;

//const uint32_t MAXTARGETS = 128;
//const uint32_t MAXTARGETS = 800;
const uint32_t MAXTARGETS = 2040;
//const uint32_t MAXTARGETS = 2048;
//const uint32_t MAXTARGETS = 3743;

//const uint32_t NTARGETS = 100;
//const uint32_t NTARGETS = 800;
const uint32_t NTARGETS = 2040;

extern "C" {
  __global__ void kmer_insert(const uint8_t *, const uint32_t *, const uint32_t, const uint32_t, uint32_t *);
  __global__ void kmer_minimizer(uint8_t *, const uint32_t *, uint32_t *, const uint32_t, const uint32_t, const uint64_t,
                                 const uint32_t, const uint32_t *, uint32_t *, uint32_t *, uint32_t *);///////
  __global__ void kmer_search(const uint8_t *, const uint32_t *, uint32_t *, const uint32_t, const uint32_t, const uint64_t,
                              const uint32_t, const uint32_t *, uint32_t *, uint32_t *, uint32_t *);
  __global__ void bloom_state(const uint32_t *, uint32_t *);
  __global__ void kmer_seeds(const uint8_t *, const uint32_t *, const uint8_t *, const uint32_t *, const uint32_t *, uint32_t *, uint32_t *, uint8_t *,
                             const uint32_t, const uint32_t , const uint32_t);

  __device__ uint32_t crc32(uint8_t *);

  __device__ void radix_sort(uint32_t *, uint32_t *, uint32_t **, int32_t);
  __device__ void radix_sort_by_key(uint32_t *, uint32_t *, uint32_t *, uint32_t **, int32_t);

  __device__ uint32_t warpReduceMin(uint32_t);
  __device__ uint32_t warpReduceMax(uint32_t);
  __device__ uint32_t blockReduceMin(uint32_t);
  __device__ uint32_t blockReduceMax(uint32_t);
  __device__ uint32_t blockReduceSum(uint32_t);
  __device__ uint32_t blockReduceMaxpos(uint32_t, uint32_t);

  int32_t uint_sort(const void *, const void *);//////
}
