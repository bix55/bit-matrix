#include "kmer.cuh"

extern "C" {
__global__ void kmer_insert(const uint8_t* __restrict__ dev_reads, const uint32_t* __restrict__ dev_reads_len,
                            const uint32_t istart, const uint32_t nreads, uint32_t *dev_bloom) {

  uint32_t i = istart + nreads / gridDim.x * blockIdx.x, iend = istart + nreads / gridDim.x * (blockIdx.x + 1);

  if(blockIdx.x == gridDim.x - 1)
    iend = istart + nreads;
  dev_reads += (i - istart) * MAX_READ_LEN;

  for(;i<iend;i++) {
    __shared__ uint32_t hash_table[MCNT];
    if(threadIdx.x == 0) {
      for(uint32_t k=0;k<MCNT;k++)
        hash_table[k] = 0;
      uint32_t hash = i;
      #pragma unroll
      for(uint32_t k=0;k<H;k++) {
        hash++;
        hash = crc32((uint8_t *)&hash);
        hash_table[(hash & MASK) >> 5] |= 1 << (hash & 0x1F);
        //hash_table[((hash >> HBIT) & MASK) >> 5] |= 1 << ((hash >> HBIT) & 0x1F);
      }
    }
    __syncthreads();

    int32_t j, jlen = dev_reads_len[i] - blockDim.x - K;

    if(MINIMIZER) jlen -= W;
    for(j=0;j<jlen;j+=blockDim.x) {
      if(MINIMIZER) {
        uint32_t kmer = 0;
        #pragma unroll
        for(uint32_t k=0;k<K;k++)
          kmer = (kmer << 2) | (((dev_reads[j+threadIdx.x+k] >> 1) ^ (dev_reads[j+threadIdx.x+k] >> 2)) & 3);
        uint32_t _rcmp = ((kmer & 0xAAAAAAAA) >> 1) | ((kmer & 0x55555555) << 1);
        uint32_t rcmp = (__brev(_rcmp) ^ 0xFFFFFFFF) >> (32 - K * 2);//reverse complement
        uint32_t maxkmer = kmer, maxrcmp = rcmp;
        uint32_t mask = 0xFFFFFFFF >> (32 - (K - 1) * 2);
        for(uint32_t w=0;w<W;w++) {
          kmer = ((kmer & mask) << 2) | (((dev_reads[j+threadIdx.x+K+w] >> 1) ^ (dev_reads[j+threadIdx.x+K+w] >> 2)) & 3);
          _rcmp = ((kmer & 0xAAAAAAAA) >> 1) | ((kmer & 0x55555555) << 1);
          rcmp = (__brev(_rcmp) ^ 0xFFFFFFFF) >> (32 - K * 2);
          if(kmer > maxkmer) maxkmer = kmer;
          if(rcmp > maxrcmp) maxrcmp = rcmp;
        }
        maxkmer = crc32((uint8_t *)&maxkmer) & (POW/KH-1);
        maxrcmp = crc32((uint8_t *)&maxrcmp) & (POW/KH-1);
        for(uint32_t k=0;k<MCNT;k++) {
          if(hash_table[k] != 0) {
            atomicOr(&dev_bloom[(maxkmer << MBIT) + k], hash_table[k]);
            atomicOr(&dev_bloom[(maxrcmp << MBIT) + k], hash_table[k]);
          }
        }
      }
      else {
        uint32_t kmer = 0;
        #pragma unroll
        for(uint32_t k=0;k<K;k++)
          kmer = (kmer << 2) | (((dev_reads[j+threadIdx.x+k] >> 1) ^ (dev_reads[j+threadIdx.x+k] >> 2)) & 3);
        uint32_t _rcmp = ((kmer & 0xAAAAAAAA) >> 1) | ((kmer & 0x55555555) << 1);
        uint32_t rcmp = (__brev(_rcmp) ^ 0xFFFFFFFF) >> (32 - K * 2);//reverse complement
        //minhash
        if(MHENABLE) {
          if(kmer != rcmp) {
            for(uint32_t m=0;m<MH;m++) {
              kmer = crc32((uint8_t *)&kmer) & (POW/KH-1);
              rcmp = crc32((uint8_t *)&rcmp) & (POW/KH-1);

              // uint32_t minhash = kmer > rcmp ? kmer : rcmp;//max
          
              // if(minhash == warpReduceMax(minhash))
              //   for(uint32_t k=0;k<MCNT;k++)
              //     if(hash_table[k] != 0)
              //       atomicOr(&dev_bloom[((minhash&(POW/KH-1)) << MBIT) + k], hash_table[k]);

              if(kmer == warpReduceMax(kmer))
                for(uint32_t k=0;k<MCNT;k++)
                  if(hash_table[k] != 0)
                    atomicOr(&dev_bloom[(kmer << MBIT) + k], hash_table[k]);
              if(rcmp == warpReduceMax(rcmp))
                for(uint32_t k=0;k<MCNT;k++)
                  if(hash_table[k] != 0)
                    atomicOr(&dev_bloom[(rcmp << MBIT) + k], hash_table[k]);
            }
          }
        }
        else {
          if(kmer != rcmp) {
            kmer = crc32((uint8_t *)&kmer) & (POW/KH-1);
            rcmp = crc32((uint8_t *)&rcmp) & (POW/KH-1);

            for(uint32_t k=0;k<MCNT;k++) {
              if(hash_table[k] != 0) {
                atomicOr(&dev_bloom[(kmer << MBIT) + k], hash_table[k]);
                atomicOr(&dev_bloom[(rcmp << MBIT) + k], hash_table[k]);
              }
            }
          }
        }
      }
    }
    if(threadIdx.x < jlen - j + blockDim.x) {
      if(MINIMIZER) {
        uint32_t kmer = 0;
        #pragma unroll
        for(uint32_t k=0;k<K;k++)
          kmer = (kmer << 2) | (((dev_reads[j+threadIdx.x+k] >> 1) ^ (dev_reads[j+threadIdx.x+k] >> 2)) & 3);
        uint32_t _rcmp = ((kmer & 0xAAAAAAAA) >> 1) | ((kmer & 0x55555555) << 1);
        uint32_t rcmp = (__brev(_rcmp) ^ 0xFFFFFFFF) >> (32 - K * 2);//reverse complement
        uint32_t maxkmer = kmer, maxrcmp = rcmp;
        uint32_t mask = 0xFFFFFFFF >> (32 - (K - 1) * 2);
        for(uint32_t w=0;w<W;w++) {
          kmer = ((kmer & mask) << 2) | (((dev_reads[j+threadIdx.x+K+w] >> 1) ^ (dev_reads[j+threadIdx.x+K+w] >> 2)) & 3);
          _rcmp = ((kmer & 0xAAAAAAAA) >> 1) | ((kmer & 0x55555555) << 1);
          rcmp = (__brev(_rcmp) ^ 0xFFFFFFFF) >> (32 - K * 2);
          if(kmer > maxkmer) maxkmer = kmer;
          if(rcmp > maxrcmp) maxrcmp = rcmp;
        }
        maxkmer = crc32((uint8_t *)&maxkmer) & (POW/KH-1);
        maxrcmp = crc32((uint8_t *)&maxrcmp) & (POW/KH-1);
        for(uint32_t k=0;k<MCNT;k++) {
          if(hash_table[k] != 0) {
            atomicOr(&dev_bloom[(maxkmer << MBIT) + k], hash_table[k]);
            atomicOr(&dev_bloom[(maxrcmp << MBIT) + k], hash_table[k]);
          }
        }
      }
      else {
        uint32_t kmer = 0;
#pragma unroll
        for(uint32_t k=0;k<K;k++)
          kmer = (kmer << 2) | (((dev_reads[j+threadIdx.x+k] >> 1) ^ (dev_reads[j+threadIdx.x+k] >> 2)) & 3);
        uint32_t _rcmp = ((kmer & 0xAAAAAAAA) >> 1) | ((kmer & 0x55555555) << 1);
        uint32_t rcmp = (__brev(_rcmp) ^ 0xFFFFFFFF) >> (32 - K * 2);//reverse complement
        //minhash
        if(MHENABLE) {
          if(kmer != rcmp) {
            for(uint32_t m=0;m<MH;m++) {
              kmer = crc32((uint8_t *)&kmer) & (POW/KH-1);
              rcmp = crc32((uint8_t *)&rcmp) & (POW/KH-1);
              if(kmer == warpReduceMax(kmer))
                for(uint32_t k=0;k<MCNT;k++)
                  if(hash_table[k] != 0)
                    atomicOr(&dev_bloom[(kmer << MBIT) + k], hash_table[k]);
              if(rcmp == warpReduceMax(rcmp))
                for(uint32_t k=0;k<MCNT;k++)
                  if(hash_table[k] != 0)
                    atomicOr(&dev_bloom[(rcmp << MBIT) + k], hash_table[k]);
            }
          }
        }
        else {
          if(kmer != rcmp) {
            kmer = crc32((uint8_t *)&kmer) & (POW/KH-1);
            rcmp = crc32((uint8_t *)&rcmp) & (POW/KH-1);

            for(uint32_t k=0;k<MCNT;k++) {
              if(hash_table[k] != 0) {
                atomicOr(&dev_bloom[(kmer << MBIT) + k], hash_table[k]);
                atomicOr(&dev_bloom[(rcmp << MBIT) + k], hash_table[k]);
              }
            }
          }
        }
      }
    }
    
    dev_reads += MAX_READ_LEN;
    __syncthreads();
  }
}

__device__ uint32_t dev_sum = 0;
__global__ void bloom_state(const uint32_t* __restrict__ dev_bloom, uint32_t *dev_state) {
  //return;
  uint32_t tid = blockDim.x * blockIdx.x + threadIdx.x;
  uint32_t count;
  __shared__ uint32_t cntmax, cntmin, cntminsum;
  __shared__ uint32_t shared_avg;
  uint32_t nbloom  = 1;

  for(uint32_t j=0;j<K-1;j++)
    nbloom *= 4;
  nbloom /= KH;
  nbloom *= 4;

  uint32_t i = nbloom / (gridDim.x * blockDim.x) * tid, iend = nbloom / (gridDim.x * blockDim.x) * (tid + 1), j;
  if(tid == gridDim.x * blockDim.x -1)
    iend = nbloom;
  uint64_t avg = 0;
  uint32_t a = 0;
  if(threadIdx.x == 0) {
    cntmax = 0;
    cntmin = 0xFFFFFFFF;
    cntminsum = 0;
    shared_avg = 0;
  }
  __syncthreads();
  for(;i<iend;i++) {
    count = 0;
    for(j=0;j<MCNT;j++)
      count += __popc(dev_bloom[i*MCNT+j]);
    atomicMax(&cntmax, count);
    atomicMin(&cntmin, count);
    if(count == 0)
      atomicAdd(&cntminsum, 1);
    avg += count; a++;
  }
  atomicAdd(&dev_sum, avg);
  avg /= a;
  atomicAdd(&shared_avg, avg);
  __syncthreads();

  dev_state += 5 * blockIdx.x;
  if(threadIdx.x == 0) {
    dev_state[0] = cntmax;
    dev_state[1] = cntmin;
    dev_state[2] = cntminsum;
    dev_state[3] = shared_avg/THREADS;
    dev_state[4] = dev_sum;
  }
  // //  if(DEBUG && threadIdx.x == 0) {
  // if(threadIdx.x == 0) {
  //   uint32_t i = nbloom / (gridDim.x * blockDim.x) * tid, iend = nbloom / (gridDim.x * blockDim.x) * (tid + 1), j;
  //   uint32_t cnt[32] = {};
  //   for(;i<iend;i++) {
  //     for(j=0;j<MCNT;j++)
  //       cnt[j/(MCNT/32)] += __popc(dev_bloom[i*MCNT+j]);
  //   }
  //   for(uint32_t k=0;k<32;k++)
  //     printf("%3u ", cnt[k]);
  //   printf("\n");
  // }
}

__device__ uint32_t dev_query_kmer_tmp[BLOCKS*MAX_READ_LEN];
// __global__ void kmer_minimizer(uint8_t* __restrict__ dev_reads, const uint32_t* __restrict__ dev_reads_len, uint32_t *dev_query_kmer,
//                                const uint32_t istart, const uint32_t nreads, const uint64_t nallreads, const uint32_t nallkmers, const uint32_t* __restrict__ dev_bloom,
//                                uint32_t *dev_targets_num, uint32_t *dev_ntargets, uint32_t *dev_min) {

//   uint32_t i = istart + nreads / gridDim.x * blockIdx.x, iend = istart + nreads / gridDim.x * (blockIdx.x + 1),
//            jstart = nallreads / blockDim.x * threadIdx.x, jend = nallreads / blockDim.x * (threadIdx.x + 1);
//   uint32_t *query_kmer = dev_query_kmer + blockIdx.x * MAX_READ_LEN;
//   uint32_t *query_kmer_tmp = dev_query_kmer_tmp + blockIdx.x * MAX_READ_LEN;

//   dev_min += i * MAX_READ_LEN;////

//   const uint32_t wid = threadIdx.x >> 5;

//   if(blockIdx.x == gridDim.x - 1)
//     iend = istart + nreads;
//   if(threadIdx.x == blockDim.x - 1)
//     jend = nallreads;

//   dev_reads += (i - istart) * MAX_READ_LEN;
//   dev_targets_num += i * MAXTARGETS;

//   for(;i<iend;i++) {//query
//     int32_t j, jlen = dev_reads_len[i] - blockDim.x - K;
//     if(MINIMIZER) jlen -= W;

//     for(j=0;j<jlen;j+=blockDim.x) {
//       if(MINIMIZER) {
//         uint32_t kmer = 0;
//         #pragma unroll
//         for(uint32_t k=0;k<K;k++)
//           kmer = (kmer << 2) | (((dev_reads[j+threadIdx.x+k] >> 1) ^ (dev_reads[j+threadIdx.x+k] >> 2)) & 3);
//         uint32_t _rcmp = ((kmer & 0xAAAAAAAA) >> 1) | ((kmer & 0x55555555) << 1);
//         uint32_t rcmp = (__brev(_rcmp) ^ 0xFFFFFFFF) >> (32 - K * 2);//reverse complement
//         uint32_t maxkmer = kmer, maxrcmp = rcmp;
//         uint32_t mask = 0xFFFFFFFF >> (32 - (K - 1) * 2);
//         for(uint32_t w=0;w<W;w++) {
//           kmer = ((kmer & mask) << 2) | (((dev_reads[j+threadIdx.x+K+w] >> 1) ^ (dev_reads[j+threadIdx.x+K+w] >> 2)) & 3);
//           _rcmp = ((kmer & 0xAAAAAAAA) >> 1) | ((kmer & 0x55555555) << 1);
//           rcmp = (__brev(_rcmp) ^ 0xFFFFFFFF) >> (32 - K * 2);
//           if(kmer > maxkmer) maxkmer = kmer;
//           if(rcmp > maxrcmp) maxrcmp = rcmp;
//         }
//         maxkmer = crc32((uint8_t *)&maxkmer) & (POW/KH-1);
//         maxrcmp = crc32((uint8_t *)&maxrcmp) & (POW/KH-1);
//         query_kmer[(j+threadIdx.x)*2] = maxkmer;
//         query_kmer[(j+threadIdx.x)*2+1] = maxrcmp;
//       }
//     }
//     if(threadIdx.x < jlen - j + blockDim.x) {
//       if(MINIMIZER) {
//         uint32_t kmer = 0;
//         #pragma unroll
//         for(uint32_t k=0;k<K;k++)
//           kmer = (kmer << 2) | (((dev_reads[j+threadIdx.x+k] >> 1) ^ (dev_reads[j+threadIdx.x+k] >> 2)) & 3);
//         uint32_t _rcmp = ((kmer & 0xAAAAAAAA) >> 1) | ((kmer & 0x55555555) << 1);
//         uint32_t rcmp = (__brev(_rcmp) ^ 0xFFFFFFFF) >> (32 - K * 2);//reverse complement
//         uint32_t maxkmer = kmer, maxrcmp = rcmp;
//         uint32_t mask = 0xFFFFFFFF >> (32 - (K - 1) * 2);
//         for(uint32_t w=0;w<W;w++) {
//           kmer = ((kmer & mask) << 2) | (((dev_reads[j+threadIdx.x+K+w] >> 1) ^ (dev_reads[j+threadIdx.x+K+w] >> 2)) & 3);
//           _rcmp = ((kmer & 0xAAAAAAAA) >> 1) | ((kmer & 0x55555555) << 1);
//           rcmp = (__brev(_rcmp) ^ 0xFFFFFFFF) >> (32 - K * 2);
//           if(kmer > maxkmer) maxkmer = kmer;
//           if(rcmp > maxrcmp) maxrcmp = rcmp;
//         }
//         maxkmer = crc32((uint8_t *)&maxkmer) & (POW/KH-1);
//         maxrcmp = crc32((uint8_t *)&maxrcmp) & (POW/KH-1);
//         query_kmer[(j+threadIdx.x)*2] = maxkmer;
//         query_kmer[(j+threadIdx.x)*2+1] = maxrcmp;
//       }
//     }
//     dev_reads += MAX_READ_LEN;
//     __syncthreads();

//     __shared__ uint32_t shared_count[M];

//     radix_sort(query_kmer, query_kmer_tmp, (uint32_t **)shared_count, (jlen+blockDim.x)*2);

//     if(threadIdx.x ==0) {
//       uint32_t uniq = query_kmer[0];
//       for(uint32_t j=1;j<(jlen+blockDim.x)*2;j++)
//         if(query_kmer[j] == uniq)
//           query_kmer[j] = 0xFFFFFFFF;
//         else
//           uniq = query_kmer[j];

//       uint32_t cnt = 0;
//       for(uint32_t j=0;j<(jlen+blockDim.x)*2;j++)
//         if(query_kmer[j] != 0xFFFFFFFF)
//           dev_min[cnt++] = query_kmer[j];
//       dev_min += MAX_READ_LEN;
//     }
//     __syncthreads();
//   }
// }

__global__ void kmer_search(const uint8_t* __restrict__ dev_reads, const uint32_t* __restrict__ dev_reads_len, uint32_t *dev_query_kmer,
                            const uint32_t istart, const uint32_t nreads, const uint64_t nallreads, const uint32_t nallkmers, const uint32_t* __restrict__ dev_bloom,
                            uint32_t *dev_targets_num, uint32_t *dev_ntargets, uint32_t *dev_min) {

  uint32_t i = istart + nreads / gridDim.x * blockIdx.x, iend = istart + nreads / gridDim.x * (blockIdx.x + 1),
           jstart = nallreads / blockDim.x * threadIdx.x, jend = nallreads / blockDim.x * (threadIdx.x + 1);
  uint32_t *query_kmer = dev_query_kmer + blockIdx.x * MAX_READ_LEN;
  uint32_t *query_kmer_tmp = dev_query_kmer_tmp + blockIdx.x * MAX_READ_LEN;

  const uint32_t wid = threadIdx.x >> 5;

  uint32_t overflow = 0;///
  if(blockIdx.x == gridDim.x - 1)
    iend = istart + nreads;
  if(threadIdx.x == blockDim.x - 1)
    jend = nallreads;

  dev_reads += (i - istart) * MAX_READ_LEN;
  dev_targets_num += i * MAXTARGETS;

  for(;i<iend;i++) {//query
    int32_t j, jlen = dev_reads_len[i] - blockDim.x - K;
    if(MINIMIZER) jlen -= W;

    for(j=0;j<jlen;j+=blockDim.x) {
      if(MINIMIZER) {
        uint32_t kmer = 0;
        #pragma unroll
        for(uint32_t k=0;k<K;k++)
          kmer = (kmer << 2) | (((dev_reads[j+threadIdx.x+k] >> 1) ^ (dev_reads[j+threadIdx.x+k] >> 2)) & 3);
        uint32_t maxkmer = kmer;
        uint32_t mask = 0xFFFFFFFF >> (32 - (K - 1) * 2);
        for(uint32_t w=0;w<W;w++) {
          kmer = ((kmer & mask) << 2) | (((dev_reads[j+threadIdx.x+K+w] >> 1) ^ (dev_reads[j+threadIdx.x+K+w] >> 2)) & 3);
          if(kmer > maxkmer) maxkmer = kmer;
        }
        maxkmer = crc32((uint8_t *)&maxkmer) & (POW/KH-1);
        query_kmer[j+threadIdx.x] = maxkmer;
      }
      else {
        uint32_t kmer = 0;
        #pragma unroll
        for(uint32_t k=0;k<K;k++)
          kmer = (kmer << 2) | (((dev_reads[j+threadIdx.x+k] >> 1) ^ (dev_reads[j+threadIdx.x+k] >> 2)) & 3);
        uint32_t _rcmp = ((kmer & 0xAAAAAAAA) >> 1) | ((kmer & 0x55555555) << 1);
        uint32_t rcmp = (__brev(_rcmp) ^ 0xFFFFFFFF) >> (32 - K * 2);//reverse complement

        //minhash
        if(MHENABLE) {
          if(kmer != rcmp) {
            for(uint32_t m=0;m<MH;m++) {
              kmer = crc32((uint8_t *)&kmer);
              if(kmer == warpReduceMax(kmer))
                query_kmer[j/32*MH+wid*MH+m] = kmer & (POW/KH-1);
            }
          }
        }
        else {
          if(kmer != rcmp)
            kmer = crc32((uint8_t *)&kmer) & (POW/KH-1);
          else
            kmer = 0xFFFFFFFF;

          query_kmer[j+threadIdx.x] = kmer;
        }
      }
    }
    if(threadIdx.x < jlen - j + blockDim.x) {
      if(MINIMIZER) {
        uint32_t kmer = 0;
        #pragma unroll
        for(uint32_t k=0;k<K;k++)
          kmer = (kmer << 2) | (((dev_reads[j+threadIdx.x+k] >> 1) ^ (dev_reads[j+threadIdx.x+k] >> 2)) & 3);
        uint32_t maxkmer = kmer;
        uint32_t mask = 0xFFFFFFFF >> (32 - (K - 1) * 2);
        for(uint32_t w=0;w<W;w++) {
          kmer = ((kmer & mask) << 2) | (((dev_reads[j+threadIdx.x+K+w] >> 1) ^ (dev_reads[j+threadIdx.x+K+w] >> 2)) & 3);
          if(kmer > maxkmer) maxkmer = kmer;
        }
        maxkmer = crc32((uint8_t *)&maxkmer) & (POW/KH-1);
        query_kmer[j+threadIdx.x] = maxkmer;
      }
      else {
        uint32_t kmer = 0;
        #pragma unroll
        for(uint32_t k=0;k<K;k++)
          kmer = (kmer << 2) | (((dev_reads[j+threadIdx.x+k] >> 1) ^ (dev_reads[j+threadIdx.x+k] >> 2)) & 3);

        uint32_t _rcmp = ((kmer & 0xAAAAAAAA) >> 1) | ((kmer & 0x55555555) << 1);
        uint32_t rcmp = (__brev(_rcmp) ^ 0xFFFFFFFF) >> (32 - K * 2);//reverse complement

        //minhash
        if(MHENABLE) {
          if(kmer != rcmp) {
            for(uint32_t m=0;m<MH;m++) {
              kmer = crc32((uint8_t *)&kmer);
              if(kmer == warpReduceMax(kmer))
                query_kmer[j/32*MH+wid*MH+m] = kmer & (POW/KH-1);
            }
          }
        }
        else {
          if(kmer != rcmp)
            kmer = crc32((uint8_t *)&kmer) & (POW/KH-1);
          else
            kmer = 0xFFFFFFFF;
          query_kmer[j+threadIdx.x] = kmer;
        }
      }
    }
    dev_reads += MAX_READ_LEN;
    __syncthreads();

    __shared__ uint32_t shared_count[M];

    // if(threadIdx.x == 0)
    //   for(uint32_t s=0;s<32;s++)
    //     shared[s] = &shared_count[256*s];

    //radix_sort(query_kmer, query_kmer_tmp, (uint32_t **)shared_count, jlen+blockDim.x);

    //sort check
    // if(threadIdx.x ==0) {
    //   for(uint32_t j=1;j<jlen+blockDim.x;j++) {
    //     if(query_kmer[j] < query_kmer[j-1]) {
    //       printf("sort err:%u\n", i);
    //       return;
    //     }
    //   }
    // }

    if(threadIdx.x ==0) {
      uint32_t uniq = query_kmer[0];
      for(uint32_t j=1;j<jlen+blockDim.x;j++)
        if(query_kmer[j] == uniq)
          query_kmer[j] = 0xFFFFFFFF;
        else
          uniq = query_kmer[j];
    }
    __syncthreads();

    // if(threadIdx.x == 0 && i == 0) {
    //   uint32_t c = 0;
    //   for(uint32_t j=0;j<jlen+blockDim.x;j++)
    //     if(query_kmer[j] != 0xFFFFFFFF)
    //       c++;
    //   printf("%u\n", c);
    // }

    uint32_t local_count[CNTIDX] = {};
    //__shared__ uint32_t shared_count[M];
    uint32_t *block_count = shared_count;
    if(threadIdx.x == 0)
      for(uint32_t k=0;k<M;k++)
        shared_count[k] = 0;
    __syncthreads();

    uint32_t qlen;
    if(MINIMIZER)
      qlen = jlen + blockDim.x;
    else {
      if(MHENABLE)
        qlen = (jlen + blockDim.x) * MH / 32;//minhash
      else
        qlen = jlen + blockDim.x;
    }
    //for(uint32_t j=0;j<qlen;j++) {
    for(uint32_t j=0;j<qlen/2;j++) {
      uint32_t kmer = query_kmer[j];
      if(kmer != 0xFFFFFFFF)
        for(uint32_t k=0;k<CNTIDX;k++)
          local_count[k] += (dev_bloom[(kmer << MBIT) + (threadIdx.x >> CNTSFT)] >> ((threadIdx.x & CNTMSK) * CNTIDX + k)) & 1;
    }
    for(uint32_t j=qlen/2;j<qlen;j++) {
      uint32_t kmer = query_kmer[j];
      if(kmer != 0xFFFFFFFF)
        for(uint32_t k=0;k<CNTIDX;k++)
          local_count[k] += ((dev_bloom[(kmer << MBIT) + (threadIdx.x >> CNTSFT)] >> ((threadIdx.x & CNTMSK) * CNTIDX + k)) & 1) << 16;
    }
    __syncthreads();

    // uint32_t local_count_min = 0xFFFFFFFF, local_count_max = 0;
    // for(uint32_t j=0;j<CNTIDX;j++) {
    //   uint32_t lower = local_count[j] & 0xFFFF;
    //   uint32_t upper = local_count[j] >> 16;
    //   if(lower < local_count_min) local_count_min = lower;
    //   else if(lower > local_count_max) local_count_max = lower;
    //   if(upper < local_count_min) local_count_min = upper;
    //   else if(upper > local_count_max) local_count_max = upper;
    // }

    // uint32_t block_count_min = blockReduceMin(local_count_min);
    // uint32_t block_count_max = blockReduceMax(local_count_max);

    // if(i==0)
    //   if(blockIdx.x == 0 && threadIdx.x == 0)
    //     printf("%u %u %u\n", block_count_max, block_count_min, block_count_max - block_count_min);

    // return;

    #pragma unroll
    for(uint32_t k=0;k<CNTIDX;k++)
      shared_count[threadIdx.x * CNTIDX + k] = local_count[k];
    __syncthreads();

    //minhash
    __shared__ uint32_t target_num[MAXTARGETS], target_score[MAXTARGETS], target_tmp[MAXTARGETS*2], ntargets;
    if(threadIdx.x == 0)
      ntargets = 0;
    __syncthreads();
    
    uint32_t min_threshold;
    if(MINIMIZER)
      min_threshold = (uint32_t)((float)nallkmers * 2 * H / ((W+1) / 2) / (POW/KH) / M * dev_reads_len[i]);
    else {
      if(MHENABLE)
        min_threshold = (uint32_t)((float)nallkmers * 2 / (float)(POW / (float)KH) / (float)M * (H) * dev_reads_len[i] / (float)(32/(float)MH) * 1.0);
      else
        min_threshold = (uint32_t)((float)nallkmers * 2 / ((float)POW / (float)KH) / (float)M * (H) * dev_reads_len[i] * 1.0);
    }
    //uint32_t forbidden = (dev_reads_len[i] - K) * 2;
    for(uint32_t j=jstart;j<jend;j++) {
      ///if(j == i) continue;
      //uint32_t minscore = 0xFFFFFFFF;
      uint32_t lower_minscore = 0xFFFFFFFF;
      uint32_t upper_minscore = 0xFFFFFFFF;

      //uint32_t maxscore = 0;
      uint32_t hash = j;
      #pragma unroll
      for(uint32_t k=0;k<H;k++) {
        hash++;
        hash = crc32((uint8_t *)&hash);
        uint32_t count = block_count[hash & MASK] & 0xFFFF;
        if(count < lower_minscore) lower_minscore = count;
        count = block_count[hash & MASK] >> 16;
        if(count < upper_minscore) upper_minscore = count;
        //if(count != forbidden) maxscore += count;
        //count = block_count[(hash >> HBIT) & MASK];
        //if(count < minscore) minscore = count;
        //if(count != forbidden) maxscore += count;
      }

      //uint32_t minscore = lower_minscore;
      uint32_t minscore = lower_minscore + upper_minscore;

      if(minscore > min_threshold) {
        uint32_t old = atomicAdd(&ntargets, 1);
        if(old >= MAXTARGETS)
          break;
        target_num[old] = j;
        target_score[old] = minscore;
      }
    }
    __syncthreads();

    if(threadIdx.x == 0)
      if(ntargets >= NTARGETS)
        ntargets = NTARGETS;
      //ntargets = 0;
    __syncthreads();

    //radix_sort_by_key(target_score, target_num, target_tmp, (uint32_t **)shared_count, ntargets);

    if(threadIdx.x == 0) {
      for(uint32_t j=0;j<ntargets;j++) {
        dev_targets_num[j] = target_num[ntargets-j-1];
        if(DEBUG)
          if(i == 1)
            printf("%u %u\n", target_num[ntargets-j-1], target_score[ntargets-j-1]);
      }
      dev_targets_num += MAXTARGETS;
      //dev_ntargets[i] = ntargets;
      dev_ntargets[i] = ntargets;
    }
    __syncthreads();
  }

  if(DEBUG)
    if(overflow > 0)
      printf("overflow:%u\n", overflow);
}

__forceinline__ __device__ uint32_t binary_search(const uint32_t* __restrict__ target, uint32_t val, uint32_t num) {

  uint32_t first = 0, last = num - 1;

  while(first != last) {
    uint32_t middle = (first + last) >> 1;
    if(target[middle] >= val)
      last = middle;
    else
      first = middle + 1;
  }
  if(target[first] == val)
    return first;
  else
    return 0xFFFFFFFF;
}

__device__ uint32_t dev_move[1000];
__device__ void kmer_aln(const uint8_t* __restrict__ dev_reads, const uint8_t* __restrict__ dev_targets,
                         const uint32_t* __restrict__ dev_reads_len, const uint32_t query_num, const uint32_t* __restrict__ dev_targets_num,
                         uint32_t ntarget, const uint32_t* __restrict__ dev_pos, uint8_t *dev_aln) {

  const uint32_t lane = threadIdx.x & 0x1F;
  const uint32_t wid = threadIdx.x >> 5;

  const int32_t g = -5;
  const uint32_t mgg = 12, xgg = 7;//0000 1100, 0000 0111
  const uint32_t mggv = mgg * 0x01010101, xggv = xgg * 0x01010101;
  const uint32_t mcv = 0x03030303, xcv = 0x02020202, icv = 0x01010101;

  for(uint32_t n=0;n<ntarget;n+=32) {
    if(n + wid >= ntarget)
      return;

    const uint8_t *query  = dev_reads + dev_pos[n+wid*2];
    const uint8_t *target = dev_targets + (n + wid) * MAX_READ_LEN + dev_pos[n+wid*2+1];

    //query, target表示
    if(threadIdx.x == 0) {
      printf("query:\n");
      for(uint32_t i=0;i<200;i++) {
        //      query[i] = 'A';
        printf("%c ", query[i]);
      }

      printf("\n");
      printf("target:\n");
      for(uint32_t i=0;i<200;i++) {
        //target[i] = 'A';
        printf("%c ", target[i]);
      }
      printf("\n");
    }

    uint32_t qv = 0, tv = 0, dv = 0, dh = 0;
    int32_t score[4], maxscore = 0;
    if(lane < 16) {
      #pragma unroll
      for(uint32_t i=0;i<4;i++)
        tv = (tv << 8) | target[(15-lane)*4+i];
      dh = mggv;
      #pragma unroll
      for(int32_t i=0;i<4;i++)
        score[i] = (63 - ((int32_t)lane * 4 + i)) * -(int32_t)mgg + g;
    }
    else {
      #pragma unroll
      for(int32_t i=3;i>=0;i--)
        qv = (qv << 8) | query[(lane-16)*4+i];
      tv = 0xFFFFFFFF;
      dv = mggv;
      #pragma unroll
      for(int32_t i=0;i<4;i++)
        score[i] = (((int32_t)lane -16) * 4 + i) * -(int32_t)mgg + g;
    }
    query  += 64;
    target += 64;
    //初期化終了

    uint32_t ilen;
    if(dev_reads_len[query_num] < dev_reads_len[dev_targets_num[n+wid]])
      ilen = dev_reads_len[query_num];
    else
      ilen = dev_reads_len[dev_targets_num[n+wid]];

    //再帰
    uint32_t cmp, match, max, _dv, _mxgv, _xggv;
    uint32_t maxpos = 0;
    uint32_t *move = dev_move + wid * 1000, midx = lane * 4;
    uint32_t _move[4] = {};

    for(uint32_t i=0;i<ilen;i++) {
      //右に移動
      qv = __funnelshift_rc(qv, __shfl_down(qv, 1), 8);
      if(lane == 31)
        qv = (qv & 0xFFFFFF) | (*query++ << 24);
      dh = __funnelshift_rc(dh, __shfl_down(dh, 1), 8);

      cmp = vcmpeq4(qv, tv);
      match = cmp;
      LOP3(xggv, mggv, cmp, cmp, 0xD8);
      max = vmaxu4(cmp, dv);
      max = vmaxu4(max, dh);
      _dv = dv;
      dv = max - dh;
      dh = max - _dv;

      #pragma unroll
      for(uint32_t j=0;j<4;j++) {
        score[j] += (((int32_t)dh >> (j * 8)) & 0xFF) + g;
        if(score[j] > maxscore) {
          maxscore = score[j];
          maxpos = (i << 3) | j;
        }
      }

      _mxgv = vcmpeq4(dv, 0);
      _mxgv &= icv;
      _xggv = vcmpeq4(_dv+dh, xggv);
      LOP3(_mxgv, xcv, _xggv, _mxgv, 0xD8);
      _mxgv |= match & mcv;

      #pragma unroll
      for(uint32_t j=0;j<4;j++)
        _move[j] = (_move[j] << 2) | ((_mxgv >> (8 * j)) & 3);

      //下に移動
      tv = __funnelshift_lc(__shfl_up(tv, 1), tv, 8);
      if(lane == 0)
        tv = (tv & 0xFFFFFF00) | (*target++);//lop3
      dv = __funnelshift_lc(__shfl_up(dv, 1), dv, 8);

      cmp = vcmpeq4(qv, tv);
      match = cmp;
      LOP3(xggv, mggv, cmp, cmp, 0xD8);
      max = vmaxu4(cmp, dv);
      max = vmaxu4(max, dh);
      _dv = dv;
      dv = max - dh;
      dh = max - _dv;

      for(int32_t j=0;j<4;j++) {
        score[j] += (((int32_t)dv >> (j * 8)) & 0xFF) + g;
        if(score[j] > maxscore) {
          maxscore = score[j];
          maxpos = ((i*2+1) << 2) | j;
        }
      }

      _mxgv = vcmpeq4(dv, 0);
      _mxgv &= icv;
      _xggv = vcmpeq4(_dv+dh, xggv);
      LOP3(_mxgv, xcv, _xggv, _mxgv, 0xD8);
      _mxgv |= match & mcv;

      #pragma unroll
      for(uint32_t j=0;j<4;j++)
        _move[j] = (_move[j] << 2) | ((_mxgv >> (8 * j)) & 3);

      if((i & 0x7) == 0x7) {
        for(uint32_t j=0;j<4;j++) {
          move[midx+j] = _move[j];
          _move[j] = 0;
        }
        midx += 128;
      }
    }

    uint32_t maxlane = lane;
    for(uint32_t offset = 16;offset > 0;offset >>= 1) {
      int32_t  _maxscore = __shfl_xor(maxscore, offset);
      uint32_t _maxpos   = __shfl_xor(maxpos,   offset);
      uint32_t _maxlane  = __shfl_xor(maxlane,  offset);
      if(_maxscore > maxscore) {
        maxscore = _maxscore;
        maxpos = _maxpos;
        maxlane = _maxlane;
      }
    }
    uint32_t rem = (ilen * 2) & 0xF;
    for(uint32_t j=0;j<4;j++)
      move[midx+j] = _move[j] << (32 - rem * 2);

    if(threadIdx.x == 0)
      printf("maxscore:%u maxpos(i):%u maxpos(j):%u maxlane:%u\n", maxscore, maxpos >> 2, maxpos & 3, maxlane);

    __shared__ uint8_t traceback[2][256];//4cell, 8bit
    __shared__ uint8_t traceback_len[2][256];//2, 4, 6, 8
    __shared__ uint8_t traceback_offset[2][256];//1, 2, 3, 4, 5

    if(threadIdx.x < 512) {
      uint32_t idx = threadIdx.x & 0xFF;
      uint32_t vh = threadIdx.x >> 8;
      uint8_t j, tb = 0;
      traceback_len[vh][idx] = 0;
      traceback_offset[vh][idx] = 0;
      for(j=0;j<8;j+=2) {
        uint8_t mxid = (idx >> j) & 3;
        tb = (tb << 2) | mxid;
        traceback_len[vh][idx] += 2;
        if((mxid & 2) == 0) {//mxid == 1 || mxid == 0
          traceback_offset[vh][idx] += 1;
          if(vh && ((mxid + (j>>1)) & 1)) {//(mxid, j) = (1, 0), (1, 4), (0, 2), [(0, 6)]
            //横型//1//h
            //(mxid, j) = (1, 0), (1, 4)
            if(mxid)//上に出る
              traceback_offset[vh][idx] |= 1 << 4;
            else//左に出る
              traceback_offset[vh][idx] |= 2 << 4;
            break;
          }
          else if(vh == 0 && (((mxid + (j>>1)) & 1) == 0)) {//(mxid, j) = (1, 2), [(1, 6)], (0, 0), (0, 4)
            //縦型//0//v
            if(mxid)//上に出る
              traceback_offset[vh][idx] |= 1 << 4;
            else//左に出る
              traceback_offset[vh][idx] |= 2 << 4;
            break;
          }
        }
        else {
          j += 2;
          traceback_offset[vh][idx] += 2;
        }
      }
      traceback[vh][idx] = tb;
    }
    __syncthreads();


    int32_t i = maxpos >> 2;
    uint32_t j = maxpos & 3;

    //traceback
    if(lane == 0) {
      while(i>=0) {
        printf("i:%u j:%u\n", i, j);
        int8_t tpidx = (i & 0xF) * 2;//0~30
        uint32_t tb_ptr = move[(i>>4)*128+maxlane*4+j];
        uint32_t _tb_ptr = 0;
        if(tpidx <= 4 && i >= 16)
          _tb_ptr = move[((i-16)>>4)*128+maxlane*4+j];
        uint8_t tp = __funnelshift_rc(tb_ptr, _tb_ptr, 30 - tpidx) & 0xFF;

        for(int8_t k=traceback_len[(i+1)&1][tp]-1;k>=0;k--) {
          if((traceback[(i+1)&1][tp] >> k) & 1)
            printf("1");
          else
            printf("0");
          if((k&1) == 0)
            printf(" ");
        }
        printf("\n");

        i -= traceback_offset[(i+1)&1][tp] & 0xF;

        uint8_t vh = traceback_offset[(i+1)&1][tp] >> 4;
        if(vh == 1) {//上に出る
          if(j == 3) {
            j = 0;
            maxlane++;
          }
          else
            j++;
          continue;
        }
        else if(vh == 2) {//左に出る
          if(j == 0) {
            j = 3;
            maxlane--;
          }
          else
            j--;
          continue;
        }
      }
    }
    __syncthreads();
  }
}

__global__ void kmer_seeds(const uint8_t* __restrict__ dev_reads, const uint32_t* __restrict__ dev_reads_len,
                           const uint8_t* __restrict__ dev_targets, const uint32_t* __restrict__ dev_targets_num, const uint32_t* __restrict__ dev_ntargets,
                           uint32_t *dev_kmer_tmp, uint32_t *dev_pos, uint8_t *dev_aln,
                           const uint32_t istart, const uint32_t nreads, const uint32_t nallreads) {

  uint32_t i = istart + nreads / gridDim.x * blockIdx.x, iend = istart + nreads / gridDim.x * (blockIdx.x + 1);

  if(blockIdx.x == gridDim.x - 1)
    iend = istart + nreads;

  dev_reads += (i - istart) * MAX_READ_LEN;
  dev_targets += (i - istart) * 64 * MAX_READ_LEN;
  dev_targets_num += i * 64;
  dev_pos += i * 128;

  uint32_t *query_kmer     = dev_kmer_tmp + blockIdx.x * MAX_READ_LEN;
  dev_kmer_tmp += BLOCKS * MAX_READ_LEN;
  uint32_t *query_kmer_pos = dev_kmer_tmp + blockIdx.x * MAX_READ_LEN;
  dev_kmer_tmp += BLOCKS * MAX_READ_LEN;
  uint32_t *query_kmer_tmp = dev_kmer_tmp + blockIdx.x * MAX_READ_LEN * 2;
  dev_kmer_tmp += BLOCKS * MAX_READ_LEN * 2;
  uint32_t *target_kmer     = dev_kmer_tmp + blockIdx.x * MAX_READ_LEN * 64;
  dev_kmer_tmp += BLOCKS * MAX_READ_LEN * 64;
  uint32_t *target_kmer_pos = dev_kmer_tmp + blockIdx.x * MAX_READ_LEN * 64;
  dev_kmer_tmp += BLOCKS * MAX_READ_LEN * 64;
  uint32_t *target_kmer_tmp = dev_kmer_tmp + blockIdx.x * MAX_READ_LEN * 64 * 2;
  dev_kmer_tmp += BLOCKS * MAX_READ_LEN * 64 * 2;
  uint32_t *seeds_count     = dev_kmer_tmp + blockIdx.x * MAX_READ_LEN * 64 * 2;

  for(;i<iend;i++) {
    //query kmer sort
    int32_t j, qlen = dev_reads_len[i] - blockDim.x - K;
    for(j=0;j<qlen;j+=blockDim.x) {
      uint32_t kmer = 0;
      #pragma unroll
      for(uint32_t k=0;k<K;k++)
        kmer = (kmer << 2) | ((dev_reads[j+threadIdx.x+k] >> 1) & 3);
      query_kmer[j+threadIdx.x] = kmer;
      query_kmer_pos[j+threadIdx.x] = j + threadIdx.x;
    }
    if(threadIdx.x < qlen - j + blockDim.x) {
      uint32_t kmer = 0;
      #pragma unroll
      for(uint32_t k=0;k<K;k++)
        kmer = (kmer << 2) | ((dev_reads[j+threadIdx.x+k] >> 1) & 3);
      query_kmer[j+threadIdx.x] = kmer;
      query_kmer_pos[j+threadIdx.x] = j + threadIdx.x;
   }
    __syncthreads();
    __shared__ uint32_t bin[32][256];
    radix_sort_by_key(query_kmer, query_kmer_pos, query_kmer_tmp, (uint32_t **)bin, qlen+blockDim.x);

    //target kmer sort
    uint32_t ntarget = dev_ntargets[i];
    uint32_t tlensum = 0;
    const uint8_t *_dev_targets = dev_targets;
    for(uint32_t n=0;n<ntarget;n++) {
      uint32_t tnum  = dev_targets_num[n];
      int32_t j, tlen = dev_reads_len[tnum] - blockDim.x - K;
      for(j=0;j<tlen;j+=blockDim.x) {
        uint32_t kmer = 0;
        #pragma unroll
        for(uint32_t k=0;k<K;k++)
          kmer = (kmer << 2) | ((_dev_targets[j+threadIdx.x+k] >> 1) & 3);
        target_kmer[tlensum+j+threadIdx.x] = kmer;
        target_kmer_pos[tlensum+j+threadIdx.x] = (n << 26) | (j + threadIdx.x);
      }
      if(threadIdx.x < tlen - j + blockDim.x) {
        uint32_t kmer = 0;
       #pragma unroll
        for(uint32_t k=0;k<K;k++)
          kmer = (kmer << 2) | ((_dev_targets[j+threadIdx.x+k] >> 1) & 3);
        target_kmer[tlensum+j+threadIdx.x] = kmer;
        target_kmer_pos[tlensum+j+threadIdx.x] = (n << 26) | (j + threadIdx.x);
     }
      _dev_targets += MAX_READ_LEN;
      tlensum += tlen + blockDim.x;
    }
    __syncthreads();
    radix_sort_by_key(target_kmer, target_kmer_pos, target_kmer_tmp, (uint32_t **)bin, tlensum);

    //seed search
    if(tlensum != 0) {
      int32_t slen = MAX_READ_LEN * 128 - blockDim.x;
      for(j=0;j<slen;j+=blockDim.x)
        seeds_count[j+threadIdx.x] = 0;
      if(threadIdx.x < slen - j + blockDim.x)
        seeds_count[j+threadIdx.x] = 0;
      __syncthreads();

      for(j=0;j<qlen;j+=blockDim.x) {
        uint32_t tstart = binary_search(target_kmer, query_kmer[j+threadIdx.x], tlensum);
        if(tstart != 0xFFFFFFFF) {
          uint32_t qpos = query_kmer_pos[j+threadIdx.x];
          uint32_t tkmer = target_kmer[tstart];
          while(1) {
            uint32_t tpos = target_kmer_pos[tstart];
            int32_t pos = qpos - (tpos & 0x3FFFFFF);
            uint32_t spos = (tpos >> 26) * MAX_READ_LEN * 2 + pos + MAX_READ_LEN;

            atomicInc(&seeds_count[spos], 0xFFFFFFFF);
            if((++tstart == tlensum) || (target_kmer[tstart] != tkmer))
              break;
          }
        }
      }
      if(threadIdx.x < qlen - j + blockDim.x) {
        uint32_t tstart = binary_search(target_kmer, query_kmer[j+threadIdx.x], tlensum);
        if(tstart != 0xFFFFFFFF) {
          uint32_t qpos = query_kmer_pos[j+threadIdx.x];
          uint32_t tkmer = target_kmer[tstart];
          while(1) {
            uint32_t tpos = target_kmer_pos[tstart];
            int32_t pos = qpos - (tpos & 0x3FFFFFF);
            uint32_t spos = (tpos >> 26) * MAX_READ_LEN * 2 + pos + MAX_READ_LEN;
            atomicInc(&seeds_count[spos], 0xFFFFFFFF);
            if((++tstart == tlensum) || (target_kmer[tstart] != tkmer))
              break;
          }
        }
      }
      __syncthreads();

      uint32_t *_seeds_count = seeds_count;
      __shared__ uint32_t seed_pos[64];
      if(threadIdx.x == 0)
        for(uint32_t k=0;k<64;k++)
          seed_pos[k] = 0;
      __syncthreads();

      for(uint32_t n=0;n<ntarget;n++) {
        int32_t slen = MAX_READ_LEN * 2 - blockDim.x;
        uint32_t s, spos, smax = 0;
        for(j=0;j<slen;j+=blockDim.x) {
          s = _seeds_count[j+threadIdx.x];
          uint32_t _spos = blockReduceMaxpos(s, j + threadIdx.x);
          if(_seeds_count[_spos] > smax) {
            smax = _seeds_count[_spos];
            spos = _spos;
          }
        }
        if(threadIdx.x < slen - j + blockDim.x)
          s = _seeds_count[j+threadIdx.x];
        else
          s = 0;
        uint32_t _spos = blockReduceMaxpos(s, j + threadIdx.x);
        if(_seeds_count[_spos] > smax)
          spos = _spos;

        if(threadIdx.x == 0)
          seed_pos[n] = spos;
        _seeds_count += MAX_READ_LEN * 2;
      }

      __shared__ uint32_t qtpos[64];
      if(threadIdx.x == 0)
        for(uint32_t k=0;k<64;k++)
          qtpos[k] = 0xFFFFFFFF;
      __syncthreads();
      for(j=0;j<qlen;j+=blockDim.x) {
        uint32_t tstart = binary_search(target_kmer, query_kmer[j+threadIdx.x], tlensum);
        if(tstart != 0xFFFFFFFF) {
          uint32_t qpos = query_kmer_pos[j+threadIdx.x];
          uint32_t tkmer = target_kmer[tstart];
          while(1) {
            uint32_t tpos = target_kmer_pos[tstart];
            int32_t pos = qpos - (tpos & 0x3FFFFFF);

            if(seed_pos[tpos>>26] == pos + MAX_READ_LEN)
              atomicMin(&qtpos[tpos>>26], qpos);

            if((++tstart == tlensum) || (target_kmer[tstart] != tkmer))
              break;
          }
        }
      }
      if(threadIdx.x < qlen - j + blockDim.x) {
        uint32_t tstart = binary_search(target_kmer, query_kmer[j+threadIdx.x], tlensum);
        if(tstart != 0xFFFFFFFF) {
          uint32_t qpos = query_kmer_pos[j+threadIdx.x];
          uint32_t tkmer = target_kmer[tstart];
          while(1) {
            uint32_t tpos = target_kmer_pos[tstart];
            int32_t pos = qpos - (tpos & 0x3FFFFFF);

            if(seed_pos[tpos>>26] == pos + MAX_READ_LEN)
              atomicMin(&qtpos[tpos>>26], qpos);

            if((++tstart == tlensum) || (target_kmer[tstart] != tkmer))
              break;
          }
        }
      }
      __syncthreads();

      if(threadIdx.x == 0) {
        for(uint32_t n=0;n<ntarget;n++) {
          dev_pos[n*2] = qtpos[n];
          dev_pos[n*2+1] = qtpos[n] - seed_pos[n] + MAX_READ_LEN;
        }
      }
      __syncthreads();

      kmer_aln(dev_reads, dev_targets, dev_reads_len, i, dev_targets_num, ntarget, dev_pos, dev_aln);
    }

    __syncthreads();

    dev_reads += MAX_READ_LEN;
    dev_targets += MAX_READ_LEN * 64;
    dev_targets_num += 64;
    dev_pos += 128;
  }
}
}

/*
__global__ void kmer_bitmatch(const uint8_t* __restrict__ dev_reads, const uint32_t* __restrict__ dev_reads_len, uint32_t *dev_bitmatch, uint32_t num) {

  const uint8_t *s = dev_reads + num * MAX_READ_LEN;
  uint32_t slen = dev_reads_len[num], k, base;
  int32_t j, jend;
  uint32_t tidIdx = blockDim.x * blockIdx.x + threadIdx.x, tidDim = gridDim.x * blockDim.x;

  jend = slen - tidDim - K;

  for(j=0;j<jend;j+=tidDim) {
    base = 0;
    for(k=0;k<K;k++)
      base = (base << 2) | ((s[j+tidIdx+k] >> 1) & 3);
    atomicOr(&dev_bitmatch[base >> 5], 1 << (base & 0x1F));
  }
  if(tidIdx < jend - j + tidDim) {
    base = 0;
    for(k=0;k<K;k++)
      base = (base << 2) | ((s[j+tidIdx+k] >> 1) & 3);
    atomicOr(&dev_bitmatch[base >> 5], 1 << (base & 0x1F));
  }
}

__global__ void kmer_bitmatchcmp(const uint32_t* __restrict__ dev_bitmatch1, const uint32_t* __restrict__ dev_bitmatch2, const uint32_t num, uint32_t *dev_result) {

  int32_t j, jend;
  uint32_t tidIdx = blockDim.x * blockIdx.x + threadIdx.x, tidDim = gridDim.x * blockDim.x, bittmp;

  jend = 512 * 1024 / 4 - tidDim;

  for(j=0;j<jend;j+=tidDim) {
    bittmp = dev_bitmatch1[j+tidIdx] & dev_bitmatch2[j+tidIdx];
    atomicAdd(&dev_result[num], __popc(bittmp));
  }
  if(tidIdx < jend - j + tidDim) {
    bittmp = dev_bitmatch1[j+tidIdx] & dev_bitmatch2[j+tidIdx];
    atomicAdd(&dev_result[num], __popc(bittmp));
  }
}

__global__ void kmer_match(const uint8_t* __restrict__ dev_reads, const uint32_t* __restrict__ dev_reads_len, uint32_t s1num, uint32_t s2num, uint32_t *kmer, uint32_t *dev_duplicate) {

  const uint8_t *s1 = dev_reads + s1num * MAX_READ_LEN;
  uint32_t s1len = dev_reads_len[s1num], k, base;
  int32_t j, jend;
  uint32_t tidIdx = blockDim.x * blockIdx.x + threadIdx.x, tidDim = gridDim.x * blockDim.x;

  jend = s1len - tidDim - K;

  for(j=0;j<jend;j+=tidDim) {
    base = 0;
    for(k=0;k<K;k++)
      base = (base << 2) | ((s1[j+tidIdx+k] >> 1) & 3);
    kmer[j+tidIdx] = base;
  }
  if(tidIdx < jend - j + tidDim) {
    base = 0;
    for(k=0;k<K;k++)
      base = (base << 2) | ((s1[j+tidIdx+k] >> 1) & 3);
    kmer[j+tidIdx] = base;
  }
}

__global__ void kmer_duplicate(const uint32_t* __restrict__ kmer, const uint32_t* __restrict__ dev_reads_len, uint32_t s1num) {

  uint32_t i, j, count = 0, s1len = dev_reads_len[s1num] - K;

  for(i=0;i<s1len-1;i++) {
    for(j=i+1;j<s1len;j++) {
      if(kmer[i] == kmer[j]) {
        printf("%u %u %u\n", i, j, kmer[i]);
        count++;
      }
    }
  }

  if(count != 0)
    printf("%u %u\n", s1num, count);
}

__global__ void __launch_bounds__(1024, 2) kmer_match2(const uint8_t* __restrict__ dev_reads, const uint32_t* __restrict__ dev_reads_len, uint32_t s1num, uint32_t s2num, uint32_t *kmer, uint32_t *dev_duplicate) {

  uint32_t idx = 80070 / gridDim.x * blockIdx.x, idxend = 80070 / gridDim.x * (blockIdx.x + 1);
  if(blockIdx.x == gridDim.x - 1)
    idxend = 80070;

  const uint8_t *s2 = dev_reads + s2num * MAX_READ_LEN;
  uint32_t s1len = dev_reads_len[s1num], s2len = dev_reads_len[s2num], i, k, base, sum = 0;
  int32_t j, jend;

  for(;idx<idxend;idx++) {
    s2num = idx;
    s2len = dev_reads_len[s2num];
    s2 = dev_reads + s2num * MAX_READ_LEN;
    sum = 0;

    jend = s2len - blockDim.x - K;

    for(j=0;j<jend;j+=blockDim.x) {
      base = 0;
      for(k=0;k<K;k++)
        base = (base << 2) | ((s2[j+threadIdx.x+k] >> 1) & 3);

      for(i=0;i<s1len-K;i++) {
        if(kmer[i] == base) {
          sum++;
          break;
        }
      }
    }
    if(threadIdx.x < jend - j + blockDim.x) {
      base = 0;
      for(k=0;k<K;k++)
        base = (base << 2) | ((s2[j+threadIdx.x+k] >> 1) & 3);

      for(i=0;i<s1len-K;i++) {
        if(kmer[i] == base) {
          sum++;
          break;
        }
      }
    }

    sum = blockReduceSum(sum);

    if(threadIdx.x == 0)
      //   printf("%5u %5u\n", s2num, sum);
      dev_duplicate[s2num] = (sum << 17) | s2num;
  }
}
__global__ void kmer_count_old(const uint8_t* __restrict__ dev_reads, const uint32_t* __restrict__ dev_reads_len, const uint32_t istart, const uint32_t nreads, const uint32_t* __restrict__ dev_bloom, uint32_t *dev_hist, uint32_t *dev_count) {

  uint32_t i = nreads / gridDim.x * blockIdx.x, iend = nreads / gridDim.x * (blockIdx.x + 1), k, l, base, histidx, clz, cnttmp[CNTIDX];
  int32_t j, jend;

  if(blockIdx.x == gridDim.x - 1)
    iend = nreads;

  dev_reads += i * (uint32_t)MAX_READ_LEN;

  //dev_hist += blockIdx.x * 131072;// 128 * 1024
  uint32_t NODUP = 1;

  for(;i<iend;i++) {
    if(i > 20)
      return;

    jend = dev_reads_len[i+istart] - blockDim.x - K;
    for(j=0;j<jend;j+=blockDim.x) {
      base = 0;
      for(k=0;k<K;k++)
        base = (base << 2) | ((dev_reads[j+threadIdx.x+k] >> 1) & 3);

      if(NODUP)
         for(k=0;k<MCNT;k++)
           for(l=0;l<32;l++)
             atomicAdd(&dev_count[(i << HBIT) + (k << 5) + l], (dev_bloom[(base << MBIT) + k] >> l) & 1);
      else
        atomicOr(&dev_hist[(base >> 5)], 1 << (base & 0x1F));
    }
    if(threadIdx.x < jend - j + blockDim.x) {
      base = 0;
      for(k=0;k<K;k++)
        base = (base << 2) | ((dev_reads[j+threadIdx.x+k] >> 1) & 3);

      if(NODUP)
        for(k=0;k<MCNT;k++)
          for(l=0;l<32;l++)
            atomicAdd(&dev_count[(i << HBIT) + (k << 5) + l], (dev_bloom[(base << MBIT) + k] >> l) & 1);
      else
        atomicOr(&dev_hist[(base >> 5)], 1 << (base & 0x1F));
    }
    dev_reads += MAX_READ_LEN;

    if(!NODUP) {
      __syncthreads();
      for(l=0;l<CNTIDX;l++)
        cnttmp[l] = 0;
      for(j=0;j<131072;j++) {
        if((histidx = dev_hist[j]) != 0) {
          while((clz = __clz(histidx)) != 32) {
            for(l=0;l<CNTIDX;l++)
              cnttmp[l] += (dev_bloom[(((j << 5) + 31 - clz) << MBIT) + (threadIdx.x >> 2)] >> (((threadIdx.x & 0x3) << 3) + l)) & 1;//M = 8192
            //        cnttmp[l] += (dev_bloom[(((j << 5) + 31 - clz) << MBIT) + (threadIdx.x >> 3)] >> (((threadIdx.x & 0x7) << 2) + l)) & 1; //M = 4096
            histidx ^= 1 << (31 - clz);
          }
        }
      }
      for(l=0;l<CNTIDX;l++)
        atomicAdd(&dev_count[(i << HBIT) + (threadIdx.x << 3) + l], cnttmp[l]);
      //atomicAdd(&dev_count[(i << HBIT) + (threadIdx.x << 2) + l], cnttmp[l]); // 4096

      __syncthreads();
      for(j=0;j<131072;j++)
        dev_hist[j] = 0;
      __syncthreads();

    }
    //    if(i>21)
    //;      return;
  }
}
*/
