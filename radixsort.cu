#include <stdint.h>

extern "C" {
int32_t uint_sort(const void *a, const void *b) {//for debug
  return *(int32_t *)b - *(int32_t *)a;
}

__device__ void radix_sort(uint32_t *target, uint32_t *tmp, uint32_t bin[32][256], uint32_t num) {

  const int32_t lane = threadIdx.x & 0x1F;
  const int32_t wid = threadIdx.x >> 5;

  const int32_t nstart = (num >> 5) * wid;
  int32_t n, nend;
  if(wid != 31)  
    nend = (num >> 5) * (wid + 1) - 32;
  else
    nend = num - 32;

  uint32_t *target_tmp = tmp;

  for(uint32_t i=0;i<4;i++) {
    //__shared__ uint32_t bin[32][256];
    #pragma unroll
    for(uint32_t j=0;j<8;j++)
      bin[wid][j*32+lane] = 0;
    __syncthreads();

    for(n=nstart;n<nend;n+=32)
      atomicInc(&bin[wid][(target[n+lane] >> (i*8)) & 0xFF], 0xFFFFFFFF);
    if(lane < nend + 32 - n)
      atomicInc(&bin[wid][(target[n+lane] >> (i*8)) & 0xFF], 0xFFFFFFFF);
    __syncthreads();

    //prefix sum
    #pragma unroll
    for(uint32_t j=0;j<8;j++) {
      #pragma unroll
      for(uint32_t k=1;k<32;k*=2) {
	uint32_t ps = __shfl_up(bin[lane][j*32+wid], k);
	if(lane >= k)
	  bin[lane][j*32+wid] += ps;
      }
    }
    __syncthreads();
    __shared__ uint32_t offset[256];
    if(wid == 0) {
      uint32_t os = 0;
      for(uint32_t j=0;j<8;j++) {
    	os += bin[31][j*32+lane];
    	for(uint32_t k=1;k<32;k*=2) {
    	  uint32_t _os = __shfl_up(os, k);
    	  if(lane >= k)
    	    os += _os;
    	}
    	offset[j*32+lane] = os;
	os = __shfl_down(os, 31);
	if(lane != 0)
	  os = 0;
      }
    }
    __syncthreads();
    if(wid != 0)
      bin[lane][wid] += offset[wid-1];
    for(uint32_t j=1;j<8;j++)
      bin[lane][j*32+wid] += offset[j*32+wid-1];
    __syncthreads();

    for(n=nend;n>nstart;n-=32) {
      #pragma unroll
      for(int32_t j=31;j>=0;j--) {
	if(lane == j) {
          uint32_t val = target[n+j];
	  uint32_t dst = --bin[wid][(val >> (i*8)) & 0xFF];
	  target_tmp[dst] = val;
        }
      }
    }
    int32_t active = nstart - n;
    if(lane > active - 1) {
      #pragma unroll
      for(int32_t j=31;j>=active;j--) {
	if(lane == j) {
          uint32_t val = target[n+j];
	  uint32_t dst = --bin[wid][(val >> (i*8)) & 0xFF];
	  target_tmp[dst] = val;
        }
      }
    }
    __syncthreads();

    uint32_t *t = target;
    target = target_tmp;
    target_tmp = t;
  }
}

__device__ void radix_sort_by_key(uint32_t *target, uint32_t *key, uint32_t *tmp, uint32_t bin[32][256], int32_t num) {

  const int32_t lane = threadIdx.x & 0x1F;
  const int32_t wid = threadIdx.x >> 5;

  const int32_t nstart = (num >> 5) * wid;
  int32_t n, nend;
  if(wid != 31)  
    nend = (num >> 5) * (wid + 1) - 32;
  else
    nend = num - 32;

  uint32_t *target_tmp = tmp;
  uint32_t *key_tmp = tmp + num;

  for(uint32_t i=0;i<4;i++) {
    //__shared__ uint32_t bin[32][256];
    #pragma unroll
    for(uint32_t j=0;j<8;j++)
      bin[wid][j*32+lane] = 0;
    __syncthreads();

    for(n=nstart;n<nend;n+=32)
      atomicInc(&bin[wid][(target[n+lane] >> (i*8)) & 0xFF], 0xFFFFFFFF);
    if(lane < nend + 32 - n)
      atomicInc(&bin[wid][(target[n+lane] >> (i*8)) & 0xFF], 0xFFFFFFFF);
    __syncthreads();

    //prefix sum
    #pragma unroll
    for(uint32_t j=0;j<8;j++) {
      #pragma unroll
      for(uint32_t k=1;k<32;k*=2) {
	uint32_t ps = __shfl_up(bin[lane][j*32+wid], k);
	if(lane >= k)
	  bin[lane][j*32+wid] += ps;
      }
    }
    __syncthreads();
    __shared__ uint32_t offset[256];
    if(wid == 0) {
      uint32_t os = 0;
      for(uint32_t j=0;j<8;j++) {
    	os += bin[31][j*32+lane];
    	for(uint32_t k=1;k<32;k*=2) {
    	  uint32_t _os = __shfl_up(os, k);
    	  if(lane >= k)
    	    os += _os;
    	}
    	offset[j*32+lane] = os;
	os = __shfl_down(os, 31);
	if(lane != 0)
	  os = 0;
      }
    }
    __syncthreads();
    if(wid != 0)
      bin[lane][wid] += offset[wid-1];
    for(uint32_t j=1;j<8;j++)
      bin[lane][j*32+wid] += offset[j*32+wid-1];
    __syncthreads();
    for(n=nend;n>nstart;n-=32) {
      #pragma unroll
      for(int32_t j=31;j>=0;j--) {
	if(lane == j) {
          uint32_t val = target[n+j];
	  uint32_t dst = --bin[wid][(val >> (i*8)) & 0xFF];
	  target_tmp[dst] = val;
	  key_tmp[dst] = key[n+j];
        }
      }
    }
    int32_t active = nstart - n;
    if(lane > active - 1) {
      #pragma unroll
      for(int32_t j=31;j>=active;j--) {
	if(lane == j) {
          uint32_t val = target[n+j];
	  uint32_t dst = --bin[wid][(val >> (i*8)) & 0xFF];
	  target_tmp[dst] = val;
	  key_tmp[dst] = key[n+j];
        }
      }
    }
    __syncthreads();

    uint32_t *t = target;
    target = target_tmp;
    target_tmp = t;
    t = key;
    key = key_tmp;
    key_tmp = t;
  }
}
}
// __device__ void radix_sort_by_key(uint32_t *target, uint32_t *target_num, uint32_t *target_pos, uint32_t *tmp, int32_t num) {

//   const int32_t lane = threadIdx.x & 0x1F;
//   const int32_t wid = threadIdx.x >> 5;

//   const int32_t nstart = (num >> 5) * wid;
//   int32_t n, nend;
//   if(wid != 31)  
//     nend = (num >> 5) * (wid + 1) - 32;
//   else
//     nend = num - 32;

//   uint32_t *target_tmp = tmp;
//   uint32_t *target_num_tmp = tmp + num;
//   uint32_t *target_pos_tmp = tmp + num * 2;

//   for(uint32_t i=0;i<4;i++) {
//     __shared__ uint32_t bin[32][256];
//     #pragma unroll
//     for(uint32_t j=0;j<8;j++)
//       bin[wid][j*32+lane] = 0;
//     __syncthreads();

//     for(n=nstart;n<nend;n+=32)
//       atomicInc(&bin[wid][(target[n+lane] >> (i*8)) & 0xFF], 0xFFFFFFFF);
//     if(lane < nend + 32 - n)
//       atomicInc(&bin[wid][(target[n+lane] >> (i*8)) & 0xFF], 0xFFFFFFFF);
//     __syncthreads();

//     //prefix sum
//     #pragma unroll
//     for(uint32_t j=0;j<8;j++) {
//       #pragma unroll
//       for(uint32_t k=1;k<32;k*=2) {
// 	uint32_t ps = __shfl_up(bin[lane][j*32+wid], k);
// 	if(lane >= k)
// 	  bin[lane][j*32+wid] += ps;
//       }
//     }
//     __syncthreads();
//     __shared__ uint32_t offset[256];
//     if(wid == 0) {
//       uint32_t os = 0;
//       for(uint32_t j=0;j<8;j++) {
//     	os += bin[31][j*32+lane];
//     	for(uint32_t k=1;k<32;k*=2) {
//     	  uint32_t _os = __shfl_up(os, k);
//     	  if(lane >= k)
//     	    os += _os;
//     	}
//     	offset[j*32+lane] = os;
// 	os = __shfl_down(os, 31);
// 	if(lane != 0)
// 	  os = 0;
//       }
//     }
//     __syncthreads();
//     if(wid != 0)
//       bin[lane][wid] += offset[wid-1];
//     for(uint32_t j=1;j<8;j++)
//       bin[lane][j*32+wid] += offset[j*32+wid-1];
//     __syncthreads();

//     for(n=nend;n>nstart;n-=32) {
//       #pragma unroll
//       for(int32_t j=31;j>=0;j--) {
// 	if(lane == j) {
//           uint32_t val = target[n+j];
// 	  uint32_t dst = --bin[wid][(val >> (i*8)) & 0xFF];
// 	  target_tmp[dst] = val;
// 	  target_num_tmp[dst] = target_num[n+j];
// 	  target_pos_tmp[dst] = target_pos[n+j];
//         }
//       }
//     }
//     int32_t active = nstart - n;
//     if(lane > active - 1) {
//       #pragma unroll
//       for(int32_t j=31;j>=active;j--) {
// 	if(lane == j) {
//           uint32_t val = target[n+j];
// 	  uint32_t dst = --bin[wid][(val >> (i*8)) & 0xFF];
// 	  target_tmp[dst] = val;
// 	  target_num_tmp[dst] = target_num[n+j];
// 	  target_pos_tmp[dst] = target_pos[n+j];
//         }
//       }
//     }
//     __syncthreads();

//     uint32_t *t = target;
//     target = target_tmp;
//     target_tmp = t;
//     t = target_num;
//     target_num = target_num_tmp;
//     target_num_tmp = t;
//     t = target_pos;
//     target_pos = target_pos_tmp;
//     target_pos_tmp = t;
//   }
// }
//}
/*
__device__ void radix_sort(uint32_t *local_sort, uint32_t *local_sort_tmp, uint32_t *local_sort_num, uint32_t *local_sort_pos, int32_t num) {

  const int32_t lane = threadIdx.x & 0x1F;
  const int32_t wid = threadIdx.x >> 5;

  const int32_t nstart = (num >> 5) * wid;
  int32_t n, nend;
  if(wid != 31)  
    nend = (num >> 5) * (wid + 1) - 32;
  else
    nend = num - 32;

  uint32_t *target = local_sort;
  uint32_t *target_tmp = local_sort_tmp;

  for(uint32_t i=0;i<4;i++) {
    __shared__ uint32_t bin[32][256];
    #pragma unroll
    for(uint32_t j=0;j<8;j++)
      bin[wid][j*32+lane] = 0;
    __syncthreads();

    for(n=nstart;n<nend;n+=32)
      atomicInc(&bin[wid][(target[n+lane] >> (i*8)) & 0xFF], 0xFFFFFFFF);
    if(lane < nend + 32 - n)
      atomicInc(&bin[wid][(target[n+lane] >> (i*8)) & 0xFF], 0xFFFFFFFF);
    __syncthreads();

    //prefix sum
    #pragma unroll
    for(uint32_t j=0;j<8;j++) {
      #pragma unroll
      for(uint32_t k=1;k<32;k*=2) {
	uint32_t ps = __shfl_up(bin[lane][j*32+wid], k);
	if(lane >= k)
	  bin[lane][j*32+wid] += ps;
      }
    }
    __syncthreads();
    __shared__ uint32_t offset[256];
    if(wid == 0) {
      uint32_t os = 0;
      for(uint32_t j=0;j<8;j++) {
    	os += bin[31][j*32+lane];
    	for(uint32_t k=1;k<32;k*=2) {
    	  uint32_t _os = __shfl_up(os, k);
    	  if(lane >= k)
    	    os += _os;
    	}
    	offset[j*32+lane] = os;
	os = __shfl_down(os, 31);
	if(lane != 0)
	  os = 0;
      }
    }
    __syncthreads();
    if(wid != 0)
      bin[lane][wid] += offset[wid-1];
    for(uint32_t j=1;j<8;j++)
      bin[lane][j*32+wid] += offset[j*32+wid-1];
    __syncthreads();

    for(n=nend;n>nstart;n-=32) {
      #pragma unroll
      for(int32_t j=31;j>=0;j--) {
	if(lane == j) {
          uint32_t val = target[n+j];
	  uint32_t dst = --bin[wid][(val >> (i*8)) & 0xFF];
	  target_tmp[dst] = val;
        }
      }
    }
    int32_t active = nstart - n;
    if(lane > active - 1) {
      #pragma unroll
      for(int32_t j=31;j>=active;j--) {
	if(lane == j) {
          uint32_t val = target[n+j];
	  uint32_t dst = --bin[wid][(val >> (i*8)) & 0xFF];
	  target_tmp[dst] = val;
        }
      }
    }
    __syncthreads();

    uint32_t *t = target;
    target = target_tmp;
    target_tmp = t;
  }
}
*/

// __device__ uint32_t dev_sort[MAX_READ_LEN*64*BLOCKS];//256MB
// __device__ uint32_t dev_sort_num[MAX_READ_LEN*64*BLOCKS];//256MB
// __device__ uint32_t dev_sort_pos[MAX_READ_LEN*64*BLOCKS];//256MB
// __device__ uint32_t dev_sort_tmp[MAX_READ_LEN*64*BLOCKS];//256MB

// __global__ void launch_radix_sort(uint32_t *local_sort, uint32_t *local_sort_tmp, uint32_t *local_sort_num, uint32_t *local_sort_pos, int32_t rsnum) {

//   // uint32_t *local_sort     = dev_sort     + MAX_READ_LEN * 64 * blockIdx.x;
//   // uint32_t *local_sort_num = dev_sort_num + MAX_READ_LEN * 64 * blockIdx.x;
//   // uint32_t *local_sort_pos = dev_sort_pos + MAX_READ_LEN * 64 * blockIdx.x;
//   // uint32_t *local_sort_tmp = dev_sort_tmp + MAX_READ_LEN * 64 * blockIdx.x;
//   // uint32_t *const init_local_sort     = local_sort;
//   // uint32_t *const init_local_sort_num = local_sort_num;
//   // uint32_t *const init_local_sort_pos = local_sort_pos;

//   // if(threadIdx.x == 0)
//   //   radix_sort<<<1, 1024>>>(local_sort, local_sort_tmp, local_sort_num, local_sort_pos, rsnum);
//   // cudaDeviceSynchronize();

//   uint32_t ok = 1;
//   if(threadIdx.x + blockIdx.x == 0)
//     for(uint32_t i=0;i<rsnum;i++) {
//       //for(uint32_t i=0;i<10;i++)
//       if(local_sort[i] != i) {
// 	if(i<10)
// 	  printf("Error!:%u %u %0x\n", i, local_sort[i], local_sort[i]);
// 	ok = 0;
//       }
//       else {
// 	if(i<10)
// 	  printf("OK!   :%u %u %0x\n", i, local_sort[i], local_sort[i]);
//       }
//     }
  
//   if(threadIdx.x == 0)
//     if(ok)
//       printf("sort check ok\n");
//     else
//       printf("sort check error!\n");
// }


// int main() {

//   struct timespec ts1, ts2;
//   clock_gettime(CLOCK_REALTIME, &ts1);

//   //uint32_t rsnum = 2500000;
//   //  uint32_t rsnum = 1048576;
//   //uint32_t rsnum = 200000000;//iwiiwi
//   //    uint32_t rsnum = 7000;
//   uint32_t rsnum = 1024 * 1024;
//   //uint32_t rsnum = 1000000;

//   //  uint32_t rsnum = 600*1024*1024;
//   //  uint32_t rsnum = 1246;
//   uint32_t *s = (uint32_t *)calloc(rsnum, sizeof(uint32_t));
//   uint32_t *sans = (uint32_t *)calloc(rsnum, sizeof(uint32_t));
//   uint32_t *dev_s, *dev_stmp;
//   cudaMalloc((void **)&dev_s, rsnum * sizeof(uint32_t));
//   cudaMalloc((void **)&dev_stmp, rsnum * sizeof(uint32_t));
//   cudaError_t err_malloc = cudaGetLastError();
//   if(err_malloc != cudaSuccess) {
//     printf("Error! sort:%s\n", cudaGetErrorString(err_malloc));
//     return 1;
//   }

//   for(uint32_t i=0;i<rsnum;i++) {
//     s[i] = rsnum - 1 - i;
//     //s[i] = rand();
//     sans[i] = s[i];
//   }
//   cudaMemcpy(dev_s, s, rsnum * sizeof(uint32_t), cudaMemcpyHostToDevice);  
//     cudaMemset(dev_stmp, 0, rsnum * sizeof(uint32_t));


//   clock_gettime(CLOCK_REALTIME, &ts1);
//   //  radix_sort<<<1, 1024>>>(dev_s, dev_stmp, NULL, NULL, rsnum);
//   // launch_radix_sort<<<1, 1024>>>(dev_s, dev_stmp, NULL, NULL, rsnum);
//   // cudaDeviceSynchronize();

//   clock_gettime(CLOCK_REALTIME, &ts2);
//   printf("sort_time:%lf\n", ts2.tv_sec - ts1.tv_sec + (double)(ts2.tv_nsec - ts1.tv_nsec) / 1000000000.0);

//   return 0;





//   uint32_t ok = 1;
//   do {
//     cudaMemset(dev_stmp, 0, rsnum * sizeof(uint32_t));

//     // cudaEvent_t cuda_start, cuda_end;
//     // float elapsed_time = 0.0f;
//     // cudaEventCreate(&cuda_start);
//     // cudaEventCreate(&cuda_end);
//     // cudaEventRecord(cuda_start, 0);

//     clock_gettime(CLOCK_REALTIME, &ts2);
//     //  printf("malloc_time:%lf\n", ts2.tv_sec - ts1.tv_sec + (double)(ts2.tv_nsec - ts1.tv_nsec) / 1000000000.0);
//     ts1 = ts2;

//     ;;//    radix_sort<<<1, 1024>>>(dev_s, dev_stmp, NULL, NULL, rsnum);
//     cudaDeviceSynchronize();

//     // cudaEventRecord(cuda_end, 0);
//     // cudaEventSynchronize(cuda_end);
//     // cudaEventElapsedTime(&elapsed_time, cuda_start, cuda_end);
//     // cudaEventDestroy(cuda_start);
//     // cudaEventDestroy(cuda_end);

//     //thrust::sort(s, s+rsnum);


//     //qsort((void *)s, rsnum, sizeof(uint32_t), uint_sort);
//     //  printf("cuda time:%.5f\n", elapsed_time / 1000.0);
//     clock_gettime(CLOCK_REALTIME, &ts2);
//     printf("sort_time:%lf\n", ts2.tv_sec - ts1.tv_sec + (double)(ts2.tv_nsec - ts1.tv_nsec) / 1000000000.0);
//     ts1 = ts2;
//     cudaError_t err_sort = cudaGetLastError();
//     if(err_sort != cudaSuccess) {
//       printf("Error! sort:%s\n", cudaGetErrorString(err_sort));
//       return 1;
//     }

//     cudaMemcpy(s, dev_s, rsnum * sizeof(uint32_t), cudaMemcpyDeviceToHost);
//     //    qsort((void *)sans, rsnum, sizeof(uint32_t), uint_sort);
//     thrust::sort(sans, sans+rsnum);

//     uint32_t prt = 0;
//     for(uint32_t i=0;i<rsnum;i++) {
//       if(s[i] != sans[i]) {
// 	if(prt) {
// 	  printf("Error!:%4u %4u %4x\n", i, s[i], s[i]);
// 	}
// 	ok = 0;
//       }
//       else
// 	if(prt)
// 	  printf("OK!   :%4u %4u %4x\n", i, s[i], s[i]);
//     }
//     if(ok)
//       printf("sort check ok:%u\n", rsnum);
//     else {
//       printf("sort check error!:%u\n", rsnum);
//       return 0;
//     }

//     rsnum -= 1;
//   }while(rsnum);

//   return 0;
// }
