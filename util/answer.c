#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <algorithm>

const uint64_t MAX_READ_LEN = 25000;

using namespace std;

int main(int argc, char **argv) {

  if(argc != 4) {
    printf("Usage: ./answer *.co output threshold\n");
    exit(EXIT_FAILURE);
  }

  FILE *fp;

  int32_t *start = (int32_t *)calloc(1000000000, sizeof(int32_t));
  int32_t *end = (int32_t *)calloc(1000000000, sizeof(int32_t));
  uint8_t buf[100];
  if((fp = fopen(argv[1], "r")) == NULL) {//reference
    perror("fopen:*.co");
    exit(EXIT_FAILURE);
  }
  uint32_t nr = 0;
  while(fgets((char*)buf, MAX_READ_LEN, fp) != NULL) {
    start[nr] = atoi(strtok((char *)buf, "\t"));
    end[nr] = atoi(strtok(NULL, "\t"));
    nr++;
  }
  fclose(fp);
  
  if((fp = fopen(argv[2], "r")) == NULL) {
    perror("fopen:kmer output");
    exit(EXIT_FAILURE);
  }

  int64_t *output = (int64_t *)calloc(1000000000, sizeof(int64_t));

  uint32_t nopt = 0;
  while(fgets((char*)buf, MAX_READ_LEN, fp) != NULL) {//output of minimap/BitMatrix
    output[nopt] = (int64_t)atoi(strtok((char*)buf, "\t")) << 32;
    output[nopt] |= atoi(strtok(NULL, "\t"));
    nopt++;
  }
  fclose(fp);

  int32_t threshold = atoi(argv[3]);
  int64_t *answer = (int64_t *)calloc(1000000000, sizeof(int64_t));
  uint32_t a = 0;
  for(uint32_t i=0;i<nr;i++) {
    for(uint32_t j=0;j<nr;j++) {
      if(i==j) continue;
      int32_t st = start[i] > start[j] ? start[i] : start[j];
      int32_t ed = end[i] < end[j] ? end[i] : end[j];      
      if(ed - st > threshold) {
        answer[a++] = (int64_t)i<<32 | j;
        //printf("%u\t%u\n", i, j);
      }
    }
  }


  /* for(uint32_t i=0;i<a;i++) */
  /*   printf("%u\t%u\n", answer[i] >> 32, answer[i] & 0xFFFFFFFF); */
  /* return 0; */

  sort(answer, answer+a);
  sort(output, output+nopt);

  int64_t *oknum = (int64_t *)calloc(1000000, sizeof(int64_t));
  int64_t *ngnum = (int64_t *)calloc(1000000, sizeof(int64_t));

  uint32_t ok = 0, ng = 0;
  for(uint32_t k=0;k<a;k++) {
    if(binary_search(output, output+nopt, answer[k])) {
      ok++;
      oknum[answer[k]>>32]++;
    }
    else {
      ng++;
      ngnum[answer[k]>>32]++;
      //      printf("ng:%u %u\n", answer[k] >> 32, answer[k] & 0xFFFFFFFF);
    }
  }

  printf("%u\t%u\t%5.2lf\n", ok, ok+ng, (double)ok/(ok+ng)*100.0);

  return 0;
  uint32_t zero = 0;
  for(uint32_t k=0;k<nr;k++)
    //if(oknum[k] + ngnum[k] != 0)
      printf("%u\t%u\t%u\t%5.2lf\t\n", k, oknum[k], oknum[k]+ngnum[k], (double)oknum[k] * 100.0 / (double)(oknum[k] + ngnum[k]));
    /* else */
    /*   printf("%u\t%u\t%u\t%5.2lf\t\n", k, oknum[k], oknum[k]+ngnum[k], zero); */

  return 0;
    




  /* uint32_t ok = 0, ng = 0, _ok = 0; */
  /* uint32_t max = 0, tmp = 0, count = 1, maxnum = 0; */
  /* for(uint32_t i=0;i<nr;i++) { */
  /*   if(s1[i] == s2[i]) continue; */
  /*   if(coordinate[s1[i]] != 0 && coordinate[s2[i]] != 0) { */
  /*     if(tmp == s1[i]) */
  /*       count++; */
  /*     else { */
  /*       if(count > max) { */
  /*         max = count; */
  /*         maxnum = s1[i] - 1; */
  /*       } */
  /*       //printf("%u %u\n", s1[i]-1, count); */
  /*       double ansrate = (double)(ok-_ok)/count*100.0; */
  /*       if(argc == 3 && atoi(argv[2]) == 3) */
  /*         if(ansrate < 90.0) */
  /*           printf("%u\t%u\t%u\t%4.1lf\n", tmp, ok-_ok, count, ansrate); */
  /*       _ok = ok; */
  /*       count = 1; */
  /*     } */
  /*     int32_t distance = coordinate[s2[i]] - coordinate[s1[i]]; */
  /*     //if(distance < len[s1[i]] * 0.5 && distance > -len[s1[i]] * 0.5) { */
  /*     if(distance < 25000 && distance > -25000) { */
  /*       ok++; */
  /*       if(argc == 3 && (atoi(argv[2]) == 0 || atoi(argv[2]) == 2)) */
  /*         printf("○ %u\t%u\n", s1[i], s2[i]); */
  /*     } */
  /*     else { */
  /*       ng++; */
  /*       if(argc == 3 && (atoi(argv[2]) == 1 || atoi(argv[2]) == 2)) */
  /*         printf("× %u\t%u\n", s1[i], s2[i]); */
  /*     } */
  /*     tmp = s1[i]; */
  /*   } */
  /* } */

  /* printf("%u %u %u\n", ok, ng, ok+ng); */
  /* printf("%lf\n", ok*100.0/(ok+ng)); */
  /* printf("%u %u\n", max+2, maxnum); */

  return 0;
}
