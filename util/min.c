#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <algorithm>

const uint64_t MAX_READ_LEN = 25000;

using namespace std;

int main(int argc, char **argv) {

  if(argc != 2) {
    printf("Usage: ./min *minimizer\n");
    exit(EXIT_FAILURE);
  }

  FILE *fp;

  uint32_t *num = (uint32_t *)calloc(100000000, sizeof(uint32_t));
  uint32_t *mlen = (uint32_t *)calloc(100000000, sizeof(uint32_t));
  uint32_t *len = (uint32_t *)calloc(100000000, sizeof(uint32_t));
  uint32_t *minimizer = (uint32_t *)calloc(100000000, sizeof(uint32_t));

  uint8_t buf[100];
  if((fp = fopen(argv[1], "r")) == NULL) {//reference
    perror("fopen:*.co");
    exit(EXIT_FAILURE);
  }
  uint32_t nr = 0;
  while(fgets((char*)buf, MAX_READ_LEN, fp) != NULL) {
    num[nr] = atoi(strtok((char *)buf, "\t"));
    mlen[nr] = atoi(strtok(NULL, "\t"));
    len[nr] = atoi(strtok(NULL, "\t"));
    minimizer[nr] = atoi(strtok(NULL, "\t"));
    nr++;
  }
  fclose(fp);

  uint32_t nreads =  num[nr-1] + 1;

  uint32_t *num2 = (uint32_t *)calloc(100000000, sizeof(uint32_t));
  uint32_t mlen2[5000];

  uint32_t *_num2 = num2;
  uint32_t *_num = num, *_mlen = mlen, *_len = len, *_minimizer = minimizer;
  for(uint32_t i=0;i<nreads;i++) {
    for(uint32_t j=0;j<_mlen[0];j++)
      _num2[j] = _minimizer[j];
    _num2 += 5000;
    mlen2[i] = _mlen[0];
    
    uint32_t offset = _mlen[0];
    _num += offset;
    _mlen += offset;
    _len += offset;
    _minimizer += offset;
  }

  _num2 = num2;

  uint32_t *result = (uint32_t *)calloc(5000*5000, sizeof(uint32_t));

  for(uint32_t i=0;i<nreads;i++) {//query
    fprintf(stderr, "%u processed\r", i);
    for(uint32_t j=0;j<mlen2[i];j++) {//query minimizer
      //printf("%u\n", _num2[j]);

      for(uint32_t k=0;k<nreads;k++) {//target
        if(binary_search(&_num2[k*5000], &_num2[k*5000]+mlen2[k], _num2[i*5000+j]))
          result[i*5000+k]++;
      }
    }
  }
  for(uint32_t i=0;i<nreads;i++) {//query
    fprintf(stderr, "%u result printed\r", i);
    for(uint32_t k=0;k<nreads;k++) {//target
      printf("%u\t%u\t%u\n", i, k, result[i*5000+k]);
    }
  }

  return 0;
}
