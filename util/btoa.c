#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

int main(int argc, char **argv) {

  if(argc != 3) {
    printf("usage: ./btoa k-mer 2bit-pack\n");
    return 1;
  }

  uint32_t K = atoi(argv[1]);
  uint32_t base = atoi(argv[2]);

  char ascii[16];

  for(uint32_t i=0;i<K;i++) {
    switch((base >> ((K-1-i)*2)) & 3) {
    case 0:
      ascii[i] = 'A';
      break;
    case 1:
      ascii[i] = 'C';
      break;
    case 2:
      ascii[i] = 'G';
      break;
    case 3:
      ascii[i] = 'T';
      break;
    }
  }
  ascii[K] = '\n';

  printf("%s", ascii);

  return 0;
}
