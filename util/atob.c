#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

int main(int argc, char **argv) {

  if(argc != 2) {
    printf("usage: ./atob ascii\n");
    return 1;
  }

  uint32_t i, base = 0;

  for(i=0;i<14;i++)
    base = (base << 2) | (((argv[1][i] >> 1) ^ (argv[1][i] >> 2)) & 3);

  printf("%u\n", base);

  return 0;
}
