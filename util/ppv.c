#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>

const uint64_t MAX_READ_LEN = 25000;
const uint64_t MAX_READ_NUM = 1000000000;
int main(int argc, char **argv) {

  FILE *fp;

  int32_t *coordinate = (int32_t *)calloc(MAX_READ_NUM, sizeof(int32_t));
  uint8_t buf[100];
  if((fp = fopen(argv[1], "r")) == NULL) {
    perror("fopen:*.co");
    exit(EXIT_FAILURE);
  }
  uint32_t nr = 0;
  while(fgets((char*)buf, MAX_READ_LEN, fp) != NULL) {
    if(nr >= MAX_READ_NUM) {
      printf("Exceeds MAX_READ_NUM\n");
      return 1;
    }
    coordinate[nr++] = atoi(strtok((char *)buf, "\t"));
  }
  fclose(fp);

  if((fp = fopen(argv[2], "r")) == NULL) {
    perror("fopen:*.output");
    exit(EXIT_FAILURE);
  }

  int32_t *s1 = (int32_t *)calloc(1000000000, sizeof(int32_t));
  int32_t *s2 = (int32_t *)calloc(1000000000, sizeof(int32_t));

  nr = 0;
  while(fgets((char*)buf, MAX_READ_LEN, fp) != NULL) {
    s1[nr] = atoi(strtok((char*)buf, "\t"));
    s2[nr] = atoi(strtok(NULL, "\t"));
    nr++;
  }
  fclose(fp);

  uint32_t ok = 0, ng = 0, _ok = 0;
  uint32_t max = 0, tmp = 0, count = 1, maxnum = 0;
  for(uint32_t i=0;i<nr;i++) {
    if(s1[i] == s2[i]) continue;
    if(coordinate[s1[i]] != 0 && coordinate[s2[i]] != 0) {
      if(tmp == s1[i])
        count++;
      else {
        if(count > max) {
          max = count;
          maxnum = s1[i] - 1;
        }
        //printf("%u %u\n", s1[i]-1, count);
        double ansrate = (double)(ok-_ok)/count*100.0;
        if(argc == 3 && atoi(argv[2]) == 3)
          if(ansrate < 90.0)
            printf("%u\t%u\t%u\t%4.1lf\n", tmp, ok-_ok, count, ansrate);
        _ok = ok;
        count = 1;
      }
      int32_t distance = coordinate[s2[i]] - coordinate[s1[i]];
      //if(distance < len[s1[i]] * 0.5 && distance > -len[s1[i]] * 0.5) {
      if(distance < 25000 && distance > -25000) {
        ok++;
        if(argc == 3 && (atoi(argv[2]) == 0 || atoi(argv[2]) == 2))
          printf("○ %u\t%u\n", s1[i], s2[i]);
      }
      else {
        ng++;
        if(argc == 3 && (atoi(argv[2]) == 1 || atoi(argv[2]) == 2))
          printf("× %u\t%u\n", s1[i], s2[i]);
      }
      tmp = s1[i];
    }
  }

  printf("%u\t%u\t%5.2lf\n", ok, ok+ng, ok*100.0/(ok+ng));
  //  printf("%u %u\n", max+2, maxnum);

  return 0;
  /* for(uint32_t i=0;i<nreads;i++) { */
  /*   if(coordinate[i] == 0) continue; */
  /*   ok = 0, ng = 0; */
  /*   for(uint32_t j=0;j<nreads;j++) { */
  /*     if(i == j) continue; */
  /*     if(coordinate[j] == 0) continue; */
  /*     int32_t distance = coordinate[j] - coordinate[i]; */
  /*     if(distance < 25000 && distance > -25000) { */
  /*       ok++; */
  /*       printf("%u\t%u\n", i, j); */
  /*     } */
  /*     else */
  /*       ng++; */
  /*   } */
  /*   printf("%u %u %u %u %lf\n", i, ok, ng, ok+ng, ok*100.0/(ok+ng)); */
  /* } */

  /* return 0; */
}
