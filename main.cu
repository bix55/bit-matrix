#include "kmer.cuh"

int main(int argc, char **argv) {

  //cudaSetDevice(1);
  struct timespec ts1, ts2;
  clock_gettime(CLOCK_REALTIME, &ts1);

  FILE  *fp;
  if((fp = fopen(argv[1], "r")) == NULL) {
    //if((fp = fopen("human/human_genome", "r")) == NULL) {
    perror("fopen");
    exit(EXIT_FAILURE);
  }

  uint8_t  *reads = (uint8_t *)calloc(MAX_READ_LEN * MAX_READ_NUM, sizeof(uint8_t));
  //uint8_t *reads;
  //cudaMallocManaged((void **)&reads, MAX_READ_LEN * MAX_READ_NUM * sizeof(uint8_t));
  uint8_t  *const init_reads = reads;
  uint32_t *reads_len = (uint32_t *)calloc(MAX_READ_NUM, 4), nreads = 0;
  uint64_t nallkmers = 0;

  while(fgets((char*)reads, MAX_READ_LEN, fp) != NULL) {
    if(reads[0] == '>') continue;
    uint32_t len = strlen((char *)reads);
    if(len < K) continue;
    reads_len[nreads++] = len;
    nallkmers += len - K;
    reads += MAX_READ_LEN;
    if(nreads == MAX_READ_NUM)
      break;
  }
  fclose(fp);

  clock_gettime(CLOCK_REALTIME, &ts2);
  fprintf(stderr, "read_time:%lf\n", ts2.tv_sec - ts1.tv_sec + (double)(ts2.tv_nsec - ts1.tv_nsec) / 1000000000.0);
  ts1 = ts2;

  uint8_t  *dev_reads;
  uint32_t *dev_reads_len, *dev_bloom, *dev_query_kmer, *dev_targets_num, *dev_ntargets;
  uint64_t size = (M / 8) * pow(4, K) / KH;
  uint32_t MEMCPYSIZE = nreads > 10000 ? 10000 : nreads;

  cudaMalloc((void **)&dev_reads, MAX_READ_LEN * MEMCPYSIZE); //25MB
  cudaMalloc((void **)&dev_reads_len, nreads * sizeof(uint32_t)); //320KB

  if(UNIFIED)
    cudaMallocManaged((void **)&dev_bloom, size); //4GB
  else
    cudaMalloc((void **)&dev_bloom, size); //4GB

  cudaMalloc((void **)&dev_query_kmer, BLOCKS * MAX_READ_LEN * sizeof(uint32_t)); //2MB
  cudaMalloc((void **)&dev_targets_num, MAXTARGETS * nreads *  sizeof(uint32_t)); //160MB
  cudaMalloc((void **)&dev_ntargets,                 nreads *  sizeof(uint32_t)); //320KB
  cudaMemcpy(dev_reads_len, reads_len, nreads * sizeof(uint32_t), cudaMemcpyHostToDevice);
  cudaMemset(dev_bloom, 0, size);
  cudaMemset(dev_targets_num, 0, MAXTARGETS * nreads * sizeof(uint32_t));
  cudaMemset(dev_ntargets, 0, nreads * sizeof(uint32_t));

  cudaError_t err_malloc = cudaGetLastError();
  if(err_malloc != cudaSuccess) {
    printf("Error! malloc:%s\n", cudaGetErrorString(err_malloc));
    return 1;
  }
  clock_gettime(CLOCK_REALTIME, &ts2);
  fprintf(stderr, "malloc_time:%lf\n", ts2.tv_sec - ts1.tv_sec + (double)(ts2.tv_nsec - ts1.tv_nsec) / 1000000000.0);
  ts1 = ts2;

  //GPU_Direct
  // cudaSetDevice(0);
  // cudaDeviceEnablePeerAccess(1, 0);
  // cudaSetDevice(1);
  // cudaDeviceEnablePeerAccess(0, 0);
  // //  uint32_t *dev_bloom1;
  // //  cudaMalloc((void **)&dev_bloom1, size);
  // //  cudaSetDevice(0);
  // //  cudaMemcpyPeer(dev_bloom1, 1, dev_bloom, 0, size);
  // clock_gettime(CLOCK_REALTIME, &ts2);
  // printf("GPU_Direct_time:%lf\n", ts2.tv_sec - ts1.tv_sec + (double)(ts2.tv_nsec - ts1.tv_nsec) / 1000000000.0);
  // ts1 = ts2;

  uint32_t i, iend = nreads - MEMCPYSIZE;
  for(i=0;i<iend;i+=MEMCPYSIZE) {

    cudaMemcpy(dev_reads, init_reads + i * MAX_READ_LEN, MAX_READ_LEN * MEMCPYSIZE, cudaMemcpyHostToDevice);//stream
    kmer_insert<<<BLOCKS, THREADS>>>(dev_reads, dev_reads_len, i, MEMCPYSIZE, dev_bloom);
    cudaDeviceSynchronize();
    cudaError_t err_insert = cudaGetLastError();
    if(err_insert != cudaSuccess) {
      printf("Error! kmer_insert:%s\n", cudaGetErrorString(err_insert));
      return 1;
    }
  }
  cudaMemcpy(dev_reads, init_reads + i * MAX_READ_LEN, MAX_READ_LEN * (nreads - i), cudaMemcpyHostToDevice);
  kmer_insert<<<BLOCKS, THREADS>>>(dev_reads, dev_reads_len, i, nreads - i, dev_bloom);
  cudaDeviceSynchronize();
  cudaError_t err_insert = cudaGetLastError();
  if(err_insert != cudaSuccess) {
    printf("Error! kmer_insert:%s\n", cudaGetErrorString(err_insert));
    return 1;
  }
  clock_gettime(CLOCK_REALTIME, &ts2);
  fprintf(stderr, "insert_time:%lf\n", ts2.tv_sec - ts1.tv_sec + (double)(ts2.tv_nsec - ts1.tv_nsec) / 1000000000.0);

  double ist = ts2.tv_sec - ts1.tv_sec + (double)(ts2.tv_nsec - ts1.tv_nsec) / 1000000000.0;
  ts1 = ts2;

  uint32_t nstate = 5 * BLOCKS;
  uint32_t *dev_state, *state = (uint32_t *)calloc(nstate, sizeof(uint32_t));

  cudaMalloc((void**)&dev_state, nstate*sizeof(uint32_t));
  bloom_state<<<BLOCKS, THREADS>>>(dev_bloom, dev_state);
  cudaDeviceSynchronize();
  cudaMemcpy(state, dev_state, nstate*sizeof(uint32_t), cudaMemcpyDeviceToHost);

  uint32_t *min = (uint32_t *)malloc(5000*MAX_READ_LEN*4), *dev_min;
  cudaMalloc((void **)&dev_min, 5000*MAX_READ_LEN*4);
  cudaMemset(dev_min, 0xFF, 5000*MAX_READ_LEN*4);

  if((err_malloc = cudaGetLastError()) != cudaSuccess) {
    printf("Error! malloc:%s\n", cudaGetErrorString(err_malloc));
    return 1;
  }
  clock_gettime(CLOCK_REALTIME, &ts2);
  ts1 = ts2;

  for(i=0;i<iend;i+=MEMCPYSIZE) {
    cudaMemcpy(dev_reads, init_reads + i * MAX_READ_LEN, MAX_READ_LEN * MEMCPYSIZE, cudaMemcpyHostToDevice);
    cudaMemset(dev_query_kmer, 255, BLOCKS * MAX_READ_LEN * sizeof(uint32_t));
    kmer_search<<<BLOCKS, THREADS>>>(dev_reads, dev_reads_len, dev_query_kmer, i, MEMCPYSIZE, nreads, nallkmers, dev_bloom, dev_targets_num, dev_ntargets, dev_min);
    //kmer_minimizer<<<BLOCKS, THREADS>>>(dev_reads, dev_reads_len, dev_query_kmer, i, MEMCPYSIZE, nreads, nallkmers, dev_bloom, dev_targets_num, dev_ntargets, dev_min);

    cudaDeviceSynchronize();
    cudaError_t err_search = cudaGetLastError();
    if(err_search != cudaSuccess) {
      printf("Error! kmer_search:%s\n", cudaGetErrorString(err_search));
      return 1;
    }
    continue;
    // cudaMemcpy(score, dev_score, MEMCPYSIZE * 4, cudaMemcpyDeviceToHost);
    cudaMemcpy(min, dev_min, 80070*20*4, cudaMemcpyDeviceToHost);

    for(uint32_t j=0;j<20;j++) {
      qsort((void *)min, 80070, sizeof(uint32_t), uint_sort);
      FILE *fp;
      if((fp = popen("gnuplot", "w")) == NULL) {
        perror("popen");
        exit(EXIT_FAILURE);
      }
      fprintf(fp, "set term png size 1920,1080\n");
      //fprintf(fp, "set term png size 3840,1080\n");
      fprintf(fp, "set output \"graph/graph_%u_%02u.png\"\n", M,  j);
      //    fprintf(fp, "set output \'/dev/null\'\n");
      fprintf(fp, "set style fill solid\n");
      //    fprintf(fp, "set xrange[0:100]\n");
      fprintf(fp, "set xrange[0:80069]\n");
      //    fprintf(fp, "set yrange[0:3000]\n");
      fprintf(fp, "set xlabel \"read\"\n");
      //    fprintf(fp, "set xlabel \"top100\"\n");
      fprintf(fp, "set ylabel \"score\"\n");
      fprintf(fp, "set nokey\n");
      fprintf(fp, "plot '-' with boxes\n");

      for(uint32_t i=0;i<80070;i++)
        fprintf(fp, "%u\t%u\n", i, min[i] >> 17);

      fprintf(fp, "e\n");

      //    fprintf(fp, "pause 100\n");

      pclose(fp);
      min += 80070;
    }
    return 0;
  }
  cudaMemcpy(dev_reads, init_reads + i * MAX_READ_LEN, MAX_READ_LEN * (nreads - i), cudaMemcpyHostToDevice);
  cudaMemset(dev_query_kmer, 255, BLOCKS * MAX_READ_LEN * sizeof(uint32_t));
  kmer_search<<<BLOCKS, THREADS>>>(dev_reads, dev_reads_len, dev_query_kmer, i, nreads - i, nreads, nallkmers, dev_bloom, dev_targets_num, dev_ntargets, dev_min);
  //kmer_minimizer<<<BLOCKS, THREADS>>>(dev_reads, dev_reads_len, dev_query_kmer, i, nreads - i, nreads, nallkmers, dev_bloom, dev_targets_num, dev_ntargets, dev_min);
  cudaDeviceSynchronize();
  cudaError_t err_search = cudaGetLastError();
  if(err_search != cudaSuccess) {
    printf("Error! kmer_search:%s\n", cudaGetErrorString(err_search));
    return 1;
  }
  clock_gettime(CLOCK_REALTIME, &ts2);
  fprintf(stderr, "search_time:%lf\n", ts2.tv_sec - ts1.tv_sec + (double)(ts2.tv_nsec - ts1.tv_nsec) / 1000000000.0);
  double srch = ts2.tv_sec - ts1.tv_sec + (double)(ts2.tv_nsec - ts1.tv_nsec) / 1000000000.0;

  fprintf(stderr, "total:%lf\n", ist + srch);
  ts1 = ts2;

  cudaFree(dev_query_kmer);
  cudaFree(dev_bloom);

  MEMCPYSIZE = 500;
  iend = nreads - MEMCPYSIZE;
  uint32_t *targets_num = (uint32_t *)malloc(MAXTARGETS * nreads * sizeof(uint32_t));//stream
  uint32_t *ntargets = (uint32_t *)malloc(nreads * sizeof(uint32_t));//stream
  cudaMemcpy(targets_num, dev_targets_num, MAXTARGETS * nreads * sizeof(uint32_t), cudaMemcpyDeviceToHost);//stream
  cudaMemcpy(ntargets, dev_ntargets, nreads * sizeof(uint32_t), cudaMemcpyDeviceToHost);//stream

  uint32_t overflow = 0;
  for(uint32_t n=0;n<nreads;n++) {
    if(ntargets[n] == 0 || ntargets[n] == NTARGETS) {
      overflow++;
      //fprintf(stderr, "of:%u\n", n);
    }
  }
  fprintf(stderr, "overflow:%u\n", overflow);

  if(PRINT) {
    for(uint32_t i=0;i<nreads;i++) {
      for(uint32_t j=0;j<ntargets[i];j++) {
        printf("%u\t%u\n", i, targets_num[i*MAXTARGETS+j]);
      }
    }
  }

  uint8_t *targets = (uint8_t *)malloc(MAXTARGETS * MAX_READ_LEN * MEMCPYSIZE);
  uint32_t *pos = (uint32_t *)malloc(MAXTARGETS * 2 * nreads * sizeof(uint32_t));
  uint8_t *dev_targets, *dev_aln;
  uint32_t *dev_kmer_tmp, *dev_pos;
  cudaMalloc((void **)&dev_targets, MAXTARGETS * MAX_READ_LEN * MEMCPYSIZE);//1.6GB
  cudaMalloc((void **)&dev_kmer_tmp, BLOCKS * MAX_READ_LEN * 388 * sizeof(uint32_t));//1.6GB
  cudaMalloc((void **)&dev_pos, MAXTARGETS * 2 * nreads * sizeof(uint32_t));//80MB
  cudaMalloc((void **)&dev_aln, MAXTARGETS * MAX_READ_LEN * 2 * MEMCPYSIZE);//3.2GB

  err_malloc = cudaGetLastError();
  if(err_malloc != cudaSuccess) {
    printf("Error! malloc:%s\n", cudaGetErrorString(err_malloc));
    return 1;
  }

  uint32_t *coordinate = (uint32_t *)calloc(nreads, sizeof(uint32_t));
  uint8_t buf[100];
  if((fp = fopen("coordinate.input", "r")) == NULL) {
    //  if((fp = fopen("human/human_genome", "r")) == NULL) {
    perror("fopen");
    exit(EXIT_FAILURE);
  }
  uint32_t nr = 0;
  while(fgets((char*)buf, MAX_READ_LEN, fp) != NULL) {
    coordinate[nr++] = atoi((char *)buf);
  }
  fclose(fp);

  for(i=0;i<iend;i+=MEMCPYSIZE) {
    for(uint32_t j=0;j<MEMCPYSIZE;j++) {
      uint32_t ntarget = ntargets[i+j];
      for(uint32_t k=0;k<ntarget;k++) {
        uint32_t num = targets_num[(i+j)*64+k];
        memcpy(&targets[(j*64+k)*MAX_READ_LEN], init_reads + num * MAX_READ_LEN, reads_len[num]);
      }
    }
    cudaMemcpy(dev_targets, targets, 64 * MAX_READ_LEN * MEMCPYSIZE, cudaMemcpyHostToDevice);
    cudaMemcpy(dev_reads, init_reads + i * MAX_READ_LEN, MAX_READ_LEN * MEMCPYSIZE, cudaMemcpyHostToDevice);
    clock_gettime(CLOCK_REALTIME, &ts2);
    printf("memcpy_time:%lf\n", ts2.tv_sec - ts1.tv_sec + (double)(ts2.tv_nsec - ts1.tv_nsec) / 1000000000.0);

    kmer_seeds<<<BLOCKS, THREADS>>>(dev_reads, dev_reads_len, dev_targets, dev_targets_num, dev_ntargets, dev_kmer_tmp, dev_pos, dev_aln, i, MEMCPYSIZE, nreads);
    cudaDeviceSynchronize();
    cudaError_t err_seeds = cudaGetLastError();
    if(err_seeds != cudaSuccess) {
      printf("Error! kmer_seeds:%s\n", cudaGetErrorString(err_seeds));
      return 1;
    }
    clock_gettime(CLOCK_REALTIME, &ts2);
    printf("seeds_time:%lf\n", ts2.tv_sec - ts1.tv_sec + (double)(ts2.tv_nsec - ts1.tv_nsec) / 1000000000.0);
  }

  uint32_t jend = nreads - i;
  for(uint32_t j=0;j<jend;j++) {
    uint32_t ntarget = ntargets[i+j];
    for(uint32_t k=0;k<ntarget;k++) {
      uint32_t num = targets_num[(i+j)*64+k];
      memcpy(&targets[(j*64+k)*MAX_READ_LEN], init_reads + num * MAX_READ_LEN, reads_len[num]);
    }
  }
  cudaMemcpy(dev_targets, targets, 64 * MAX_READ_LEN * (nreads - i), cudaMemcpyHostToDevice);
  cudaMemcpy(dev_reads, init_reads + i * MAX_READ_LEN, MAX_READ_LEN * (nreads - i), cudaMemcpyHostToDevice);
  clock_gettime(CLOCK_REALTIME, &ts2);
  printf("memcpy_time:%lf\n", ts2.tv_sec - ts1.tv_sec + (double)(ts2.tv_nsec - ts1.tv_nsec) / 1000000000.0);

  kmer_seeds<<<BLOCKS, THREADS>>>(dev_reads, dev_reads_len, dev_targets, dev_targets_num, dev_ntargets, dev_kmer_tmp, dev_pos, dev_aln, i, (nreads - i), nreads);
  cudaDeviceSynchronize();
  cudaError_t err_seeds = cudaGetLastError();
  if(err_seeds != cudaSuccess) {
    printf("Error! kmer_seeds:%s\n", cudaGetErrorString(err_seeds));
    return 1;
  }
  clock_gettime(CLOCK_REALTIME, &ts2);
  printf("seeds_time:%lf\n", ts2.tv_sec - ts1.tv_sec + (double)(ts2.tv_nsec - ts1.tv_nsec) / 1000000000.0);
  ts1 = ts2;
  cudaMemcpy(pos, dev_pos, 64 * 2 * nreads * sizeof(uint32_t), cudaMemcpyDeviceToHost);

  cudaFree(dev_kmer_tmp);

  free(init_reads);
  free(reads_len);
  free(targets_num);
  free(ntargets);
  free(targets);
  //free(pos);

  cudaFree(dev_reads);
  cudaFree(dev_reads_len);
  cudaFree(dev_targets_num);
  cudaFree(dev_ntargets);
  cudaFree(dev_targets);
  cudaFree(dev_kmer_tmp);
  cudaFree(dev_pos);

  return 0;

  /*
  //kmer_bitmatch
  cudaFree(dev_bloom);
  cudaFree(dev_reads);
  cudaMalloc((void **)&dev_reads, MAX_READ_LEN * nreads);
  cudaMemcpy(dev_reads, init_reads, MAX_READ_LEN * nreads, cudaMemcpyHostToDevice);

  uint32_t *dev_bitmatch1, *dev_bitmatch2, *result = (uint32_t *)calloc(nreads, 4), *dev_result;
  cudaMalloc((void **)&dev_bitmatch1, 512 * 1024);
  cudaMalloc((void **)&dev_bitmatch2, 512 * 1024);
  cudaMalloc((void **)&dev_result, nreads * 4);

  //  for(i=3111;i<nreads;i++) {
  for(i=10000;i<nreads;i++) {
    cudaMemset(dev_bitmatch1, 0, 512 * 1024);
    kmer_bitmatch<<<BLOCKS, THREADS>>>(dev_reads, dev_reads_len, dev_bitmatch1, i);
    cudaDeviceSynchronize();
    if((err_search = cudaGetLastError()) != cudaSuccess) {
      printf("Error! kmer_bitmatch1:%s\n", cudaGetErrorString(err_search));
      return 1;
    }
    cudaMemset(dev_result, 0, nreads * 4);
    for(j=0;j<nreads;j++) {
      cudaMemset(dev_bitmatch2, 0, 512 * 1024);
      kmer_bitmatch<<<BLOCKS, THREADS>>>(dev_reads, dev_reads_len, dev_bitmatch2, j);
      cudaDeviceSynchronize();
      if((err_search = cudaGetLastError()) != cudaSuccess) {
        printf("Error! kmer_bitmatch2:%s\n", cudaGetErrorString(err_search));
        return 1;
      }
      kmer_bitmatchcmp<<<BLOCKS, THREADS>>>(dev_bitmatch1, dev_bitmatch2, j, dev_result);
      cudaDeviceSynchronize();
      if((err_search = cudaGetLastError()) != cudaSuccess) {
        printf("Error! kmer_bitmatchcmp:%s\n", cudaGetErrorString(err_search));
        return 1;
      }
      //      printf("%5u %5u\r", i, j);
    }
    cudaMemcpy(result, dev_result, nreads * 4, cudaMemcpyDeviceToHost);

    for(j=0;j<nreads;j++)
      result[j] = (result[j] << 17) | j;

    qsort((void *)result, 80070, sizeof(uint32_t), uint_sort);

    for(j=0;j<10;j++)
      printf("%5u %5u %5u\n", i, result[j] & 0x1FFFF, result[j] >> 17);
  }

  cudaFree(dev_bitmatch1);
  cudaFree(dev_bitmatch2);
  cudaFree(dev_result);
  return 0;
  //kmer_bitmatch
  */

  /*
  //kmer_match
  cudaFree(dev_reads);
  cudaMalloc((void **)&dev_reads, MAX_READ_LEN * nreads);
  cudaMemcpy(dev_reads, init_reads, MAX_READ_LEN * nreads, cudaMemcpyHostToDevice);
  uint32_t *kmer;
  cudaMalloc((void **)&kmer, 275000 * 4);

  uint32_t *duplicate = (uint32_t *)malloc(80070 * 4), *dev_duplicate;
  cudaMalloc((void **)&dev_duplicate, 80070 * 4);

  uint32_t idx = 0;
  const uint32_t range = 80070;

  for(idx=0;idx<range;idx++) {
    //    continue;
    cudaMemset(kmer, 0, 275000);
    memset(duplicate, 0, 275000);
    cudaMemset(dev_duplicate, 0, 80070);

    kmer_match<<<208, 1024>>>(dev_reads, dev_reads_len, idx, 0, kmer, dev_duplicate);
    cudaDeviceSynchronize();
    if((err_search = cudaGetLastError()) != cudaSuccess) {
      printf("Error! kmer_match:%s\n", cudaGetErrorString(err_search));
      return 1;
    }
    kmer_duplicate<<<1, 1>>>(kmer, dev_reads_len, idx);
    continue;

    kmer_match2<<<BLOCKS, THREADS>>>(dev_reads, dev_reads_len, idx, 0, kmer, dev_duplicate);
    cudaDeviceSynchronize();
    if((err_search = cudaGetLastError()) != cudaSuccess) {
      printf("Error! kmer_match2:%s\n", cudaGetErrorString(err_search));
      return 1;
    }

    cudaMemcpy(duplicate, dev_duplicate, 80070 * 4, cudaMemcpyDeviceToHost);
    // uint32_t ssum = 0;
    // for(i=0;i<range;i++) {
    //   if(duplicate[i] > 10)
    //     ;//      printf("%5u %5u\n", i, duplicate[i]);
    //   ssum += duplicate[i];
    // }
    // printf("ssum:%5u\n", ssum);

    qsort((void *)duplicate, 80070, sizeof(uint32_t), uint_sort);
    // for(i=0;i<10;i++)
    //   printf("%u\n", duplicate[i]);

    for(i=0;i<10;i++)
      printf("%5u %5u %5u\n", idx, duplicate[i] & 0x1FFFF, duplicate[i] >> 17);
  }

  cudaFree(dev_reads);
  cudaMalloc((void **)&dev_reads, MAX_READ_LEN * MEMCPYSIZE);
  return 0;
//kmer_match
*/

}
