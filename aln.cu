#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <time.h>
#include "simd_functions.h"

#define MAX_READ_LEN    25000
#define MAX_READ_ROW    100000
#define MEMCPYSIZE      10000

#define K       11              // k-mer
#define M       8192            // Bloom Filter size[bit]
#define MCNT    256             // Bloom Filter count(uint32_t)
#define MBIT    8               // Bloom Filter count bitwidth(=log(MCNT))
#define H       3               // Number of Hash function
#define HBIT    13              // Hash value bitwidth(=log(M))

#define MASK    0x1FFF          // Bloom Filter size mask(M - 1)
#define CNTIDX  8               // M / THREAD

// #define VEC_SELECT(a, b, c, d) { (a) = (b) ^ ((d) & ((b) ^ (c))); }
// #define VEC_SELECT2(a, b, c, d) { (a) = (c) ^ ((d) & ((c) ^ (b))); }

#define LOP3(A, B, C, D, E) asm("lop3.b32 %0, %1, %2, %3, "#E";" : "=r"(D) : "r"(A), "r"(B), "r"(C))

// __inline__ __device__ uint32_t warpReduceMax(uint32_t val) {
//   uint32_t shfl;
//   for(uint32_t offset = 16;offset > 0;offset >>= 1)
//     if(val < (shfl = __shfl_xor(val, offset)))
//       val = shfl;
//   return val;
// }


__device__ uint32_t dev_move[100000];
#define QPOS 375
#define TPOS 3679
extern "C" __global__ void kmer_aln(uint8_t *dev_reads, const uint32_t* __restrict__ dev_reads_len, const uint32_t istart, const uint32_t nreads, uint32_t qpos, uint32_t tpos) {
  //  extern "C" __global__ void kmer_aln(const uint8_t* __restrict__ dev_reads, const uint32_t* __restrict__ dev_reads_len, const uint32_t istart, const uint32_t nreads) {

  const uint32_t lane = threadIdx.x & 0x1F;
  const uint32_t wid = threadIdx.x >> 5;

  uint32_t q = 0, t = 3288;
  // uint8_t *query  = dev_reads + q * MAX_READ_LEN + qpos;
  // uint8_t *target = dev_reads + t * MAX_READ_LEN + tpos;
  //  const uint8_t *query  = dev_reads + q * MAX_READ_LEN;
  //  const uint8_t *target = dev_reads + t * MAX_READ_LEN;

  char que[200] = "AAAGGGCGGGCCC";
  char tar[200] = "AAGGCGGGCCT";
  uint8_t *query = (uint8_t *)que;
  uint8_t *target = (uint8_t *)tar;


  const int32_t m = 2, x = -3, g = -5;//__const__ ?
  const uint32_t mgg = 12, xgg = 7;//0000 1100, 0000 0111
  const uint32_t mggv = mgg * 0x01010101, xggv = xgg * 0x01010101;
  const uint32_t mcv = 0x03030303, xcv = 0x02020202, icv = 0x01010101;

  //query, target表示
  if(threadIdx.x == 0) {
    printf("query:\n");
    for(uint32_t i=0;i<200;i++) {
      //      query[i] = 'A';
      printf("%c ", query[i]);
    }

    printf("\n");
    printf("target:\n");
    for(uint32_t i=0;i<200;i++) {
      //target[i] = 'A';
      printf("%c ", target[i]);
    }
    printf("\n");
  }

  // uint32_t a, b, c, d, lop;
  // VEC_SELECT2(a, 0xFFFFFFFF, 0, 0xFFFF0000);
  // //  lop = 0xF0 ^ (0xAA & (0xF0 ^ 0xCC));
  // LOP3(0xFFFFCCCC, 0xBBBBAAAA, 0xAAAA0000, lop, 0xD8);

  // if(threadIdx.x == 0 && blockIdx.x == 0) {
  //   printf("%0x %0x\n", a, lop);
  // }

  uint32_t qv = 0, tv = 0, dv = 0, dh = 0;
  int32_t score[4], maxscore = 0;
  if(lane < 16) {
    //tv = ((uint32_t *)target)[15-lane];
    //tv = __byte_perm(tv, tv, 0x123);
    #pragma unroll
    for(uint32_t i=0;i<4;i++)
      tv = (tv << 8) | target[(15-lane)*4+i];

    dh = mggv;
    #pragma unroll
    for(int32_t i=0;i<4;i++)
      score[i] = (63 - ((int32_t)lane * 4 + i)) * -(int32_t)mgg + g;
  }
  else {
    //qv = ((uint32_t *)query)[lane-16];
    #pragma unroll
    for(int32_t i=3;i>=0;i--)
      qv = (qv << 8) | query[(lane-16)*4+i];

    tv = 0xFFFFFFFF;
    dv = mggv;
    #pragma unroll
    for(int32_t i=0;i<4;i++)
      score[i] = (((int32_t)lane -16) * 4 + i) * -(int32_t)mgg + g;
  }
  query  += 64;
  target += 64;
  //初期化終了

  // for(uint32_t l=0;l<32;l++) {
  //   if(lane == l) {
  //     printf("tv %2u:", lane);
  //     for(int8_t k=31;k>=0;k--) {
  //       printf("%u", (tv>>k)&1);
  //       if((k&7) == 0)
  //         printf(" ");
  //     }
  //     printf("\n");
  //   }
  // }

  // for(uint32_t l=0;l<32;l++) {
  //   if(lane == l) {
  //     printf("qv %2u:", lane);
  //     for(int8_t k=31;k>=0;k--) {
  //       printf("%u", (qv>>k)&1);
  //       if((k&7) == 0)
  //         printf(" ");
  //     }
  //     printf("\n");
  //   }
  // }

  // return;



  // //初期値表示
  // for(uint32_t j=0;j<32;j++)
  //   if(j==lane)
  //     for(uint32_t i=0;i<4;i++)
  //    printf("%c" , ((uint8_t *)&qv)[i]);
  // if(lane == 0)
  //   printf("\n");
  // for(int32_t j=31;j>=0;j--)
  //   if(j==lane)
  //     for(int32_t i=3;i>=0;i--)
  //            printf("%c", ((uint8_t *)&tv)[i]);
  // if(lane == 0)
  //   printf("\n");


  // if(lane == 0)
  //   printf("---------------------------------\n");
  // for(uint32_t j=15;j<17;j++)
  //   if(j==lane)
  //     for(uint32_t k=0;k<4;k++)
  //    printf("%3d ", score[k]);
  // if(lane == 0)
  //   printf("\n");

  uint32_t ilen;
  if(dev_reads_len[q] < dev_reads_len[t])
    ilen = dev_reads_len[q];
  else
    ilen = dev_reads_len[t];

 //再帰
  uint32_t cmp, match, max, _dv, mxgv, _mxgv, _xggv;
  uint32_t maxpos = 0;
  uint32_t *move = dev_move + wid * 1000, midx = lane * 4;
  uint32_t _move[4] = {};

  ilen = 11;//////
  for(uint32_t i=0;i<ilen;i++) {
    //右に移動
    qv = __funnelshift_rc(qv, __shfl_down(qv, 1), 8);
    if(lane == 31)
      qv = (qv & 0xFFFFFF) | (*query++ << 24);
    dh = __funnelshift_rc(dh, __shfl_down(dh, 1), 8);

    cmp = vcmpeq4(qv, tv);
    match = cmp;
    LOP3(xggv, mggv, cmp, cmp, 0xD8);
    max = vmaxu4(cmp, dv);
    max = vmaxu4(max, dh);
    _dv = dv;
    dv = max - dh;
    dh = max - _dv;

    #pragma unroll
    for(uint32_t j=0;j<4;j++) {
      score[j] += (((int32_t)dh >> (j * 8)) & 0xFF) + g;
      if(score[j] > maxscore) {
        maxscore = score[j];
        maxpos = (i << 3) | j;
      }
    }

    _mxgv = vcmpeq4(dv, 0);
    _mxgv &= icv;
    _xggv = vcmpeq4(_dv+dh, xggv);
    LOP3(_mxgv, xcv, _xggv, _mxgv, 0xD8);
    _mxgv |= match & mcv;
    // if(i&1)//lop3
    //   mxgv |= (((_mxgv >> 2) | (_mxgv << 4)) | ((_mxgv << 10) | (_mxgv << 16))) & 0xFF0000;
    // else
    //   mxgv = (((_mxgv >> 18) | (_mxgv >> 12)) | ((_mxgv >> 6) | _mxgv)) & 0xFF;

    #pragma unroll
    for(uint32_t j=0;j<4;j++)
      _move[j] = (_move[j] << 2) | ((_mxgv >> (8 * j)) & 3);

    // if(lane == 0)
    //   printf("-move right--------------------------------\n");
    // for(uint32_t j=15;j<17;j++)
    //   if(j==lane)
    //     for(uint32_t k=0;k<4;k++)
    //       printf("%3d ", score[k]);
    // if(lane == 0)
    //   printf("\n");

    //下に移動
    tv = __funnelshift_lc(__shfl_up(tv, 1), tv, 8);
    if(lane == 0)
      tv = (tv & 0xFFFFFF00) | (*target++);//lop3
    dv = __funnelshift_lc(__shfl_up(dv, 1), dv, 8);

    cmp = vcmpeq4(qv, tv);
    match = cmp;
    LOP3(xggv, mggv, cmp, cmp, 0xD8);
    max = vmaxu4(cmp, dv);
    max = vmaxu4(max, dh);
    _dv = dv;
    dv = max - dh;
    dh = max - _dv;

    for(int32_t j=0;j<4;j++) {
      score[j] += (((int32_t)dv >> (j * 8)) & 0xFF) + g;
      if(score[j] > maxscore) {
        maxscore = score[j];
        maxpos = ((i*2+1) << 2) | j;
      }
    }

    _mxgv = vcmpeq4(dv, 0);
    _mxgv &= icv;
    _xggv = vcmpeq4(_dv+dh, xggv);
    LOP3(_mxgv, xcv, _xggv, _mxgv, 0xD8);
    _mxgv |= match & mcv;

    #pragma unroll
    for(uint32_t j=0;j<4;j++)
      _move[j] = (_move[j] << 2) | ((_mxgv >> (8 * j)) & 3);

    if((i & 0x7) == 0x7) {
      for(uint32_t j=0;j<4;j++) {
        move[midx+j] = _move[j];
        _move[j] = 0;
      }
      midx += 128;
    }

    // if(lane == 0)
    //   printf("-move down--------------------------------\n");
    // for(uint32_t j=15;j<17;j++)
    //   if(j==lane)
    //     for(uint32_t k=0;k<4;k++)
    //       printf("%3d ", score[k]);
    // if(lane == 0)
    //   printf("\n");
  }

  uint32_t maxlane = lane;
  for(uint32_t offset = 16;offset > 0;offset >>= 1) {
    int32_t  _maxscore = __shfl_xor(maxscore, offset);
    uint32_t _maxpos   = __shfl_xor(maxpos,   offset);
    uint32_t _maxlane  = __shfl_xor(maxlane,  offset);
    if(_maxscore > maxscore) {
      maxscore = _maxscore;
      maxpos = _maxpos;
      maxlane = _maxlane;
    }
  }
  uint32_t rem = (ilen * 2) & 0xF;
  for(uint32_t j=0;j<4;j++)
    move[midx+j] = _move[j] << (32 - rem * 2);

  if(threadIdx.x == 0)
    printf("maxscore:%u maxpos(i):%u maxpos(j):%u maxlane:%u\n", maxscore, maxpos >> 2, maxpos & 3, maxlane);

  __shared__ uint8_t traceback[2][256];//4cell, 8bit
  __shared__ uint8_t traceback_len[2][256];//2, 4, 6, 8
  __shared__ uint8_t traceback_offset[2][256];//1, 2, 3, 4, 5

  // uint32_t idx = threadIdx.x & 0xFF;
  // for(;idx<512;idx+=32) {
  if(threadIdx.x < 512) {
    uint32_t idx = threadIdx.x & 0xFF;
    uint32_t vh = threadIdx.x >> 8;
    uint8_t j, tb = 0;
    traceback_len[vh][idx] = 0;
    traceback_offset[vh][idx] = 0;
    for(j=0;j<8;j+=2) {
      uint8_t mxid = (idx >> j) & 3;
      tb = (tb << 2) | mxid;
      traceback_len[vh][idx] += 2;
      if((mxid & 2) == 0) {//mxid == 1 || mxid == 0
        traceback_offset[vh][idx] += 1;
        if(vh && ((mxid + (j>>1)) & 1)) {//(mxid, j) = (1, 0), (1, 4), (0, 2), [(0, 6)]
          //横型//1//h
          //(mxid, j) = (1, 0), (1, 4)
          if(mxid)//上に出る
            traceback_offset[vh][idx] |= 1 << 4;
          else//左に出る
            traceback_offset[vh][idx] |= 2 << 4;
          break;
        }
        else if(vh == 0 && (((mxid + (j>>1)) & 1) == 0)) {//(mxid, j) = (1, 2), [(1, 6)], (0, 0), (0, 4)
          //縦型//0//v
          if(mxid)//上に出る
            traceback_offset[vh][idx] |= 1 << 4;
          else//左に出る
            traceback_offset[vh][idx] |= 2 << 4;
          break;
        }
      }
      else {
        j += 2;
        traceback_offset[vh][idx] += 2;
      }
    }
    //printf("%u %u\n", idx, j);
    //traceback[i] = __brev(tb) >> 24;
    traceback[vh][idx] = tb;
  }
//  }
  __syncthreads();

  if(wid != 0) return;///


  // uint8_t _vh = 0;
  // if(threadIdx.x == 0) {
  //   for(uint32_t i=0;i<256;i++) {
  //     printf("%u ", i);
  //     for(int8_t j=7;j>=0;j--)
  //       //for(uint8_t j=0;j<8;j++)
  //       if((i >> j) & 1)
  //         printf("1");
  //       else
  //         printf("0");
  //     printf(" ");
  //     for(int8_t j=traceback_len[_vh][i]-1;j>=0;j--)
  //       //for(uint8_t j=0;j<8;j++)
  //       if((traceback[_vh][i] >> j) & 1)
  //         printf("1");
  //       else
  //         printf("0");
  //     printf("\t%u\n", traceback_offset[_vh][i] & 0xF);
  //   }
  // }

  // return;

  //max:i=2, j=3
  int32_t i = maxpos >> 2;//i
  uint32_t j = maxpos & 3;//j
  //i / 2 :
  //i & 1 : right or down

  //traceback
  if(lane == 0) {

    // for(uint32_t l=0;l<130;l++) {
    //   uint32_t tb_ptr = move[l];
    //   printf("%u:", l);
    //   for(int8_t k=31;k>=0;k--) {
    //     printf("%u", (tb_ptr>>k)&1);
    //     if((k&1) == 0)
    //       printf(" ");
    //   }
    //   printf("\n");
    // }

    // return;
    while(i>=0) {
      printf("i:%u j:%u\n", i, j);
      int8_t tpidx = (i & 0xF) * 2;//0~30
      uint32_t tb_ptr = move[(i>>4)*128+maxlane*4+j];
      for(int8_t k=31;k>=0;k--) {
        printf("%u", (tb_ptr>>k)&1);
        if((k&1) == 0)
          printf(" ");
      }
      printf("\n");
      uint32_t _tb_ptr = 0;
      if(tpidx <= 4 && i >= 16)
        _tb_ptr = move[((i-16)>>4)*128+maxlane*4+j];
      uint8_t tp = __funnelshift_rc(tb_ptr, _tb_ptr, 30 - tpidx) & 0xFF;

      printf("tp:");
      for(int8_t k=7;k>=0;k--) {
        printf("%u", (tp>>k)&1);
        if((k&1) == 0)
          printf(" ");
      }
      printf("\n");


      for(int8_t k=traceback_len[(i+1)&1][tp]-1;k>=0;k--) {
        if((traceback[(i+1)&1][tp] >> k) & 1)
          printf("1");
        else
          printf("0");
        if((k&1) == 0)
          printf(" ");
      }
      printf("\n");

      i -= traceback_offset[(i+1)&1][tp] & 0xF;

      uint8_t vh = traceback_offset[(i+1)&1][tp] >> 4;
      if(vh == 1) {//上に出る
        //printf("!!out up\n");
        if(j == 3) {
          j = 0;
          maxlane++;
        }
        else
          j++;
        continue;
      }
      else if(vh == 2) {//左に出る
        //printf("!!out left\n");
        if(j == 0) {
          j = 3;
          maxlane--;
        }
        else
          j--;
        continue;
      }
    }
  }
  __syncthreads();
}

int main() {

  uint8_t       *reads  = (uint8_t *)calloc((uint32_t)MAX_READ_LEN * (uint32_t)MAX_READ_ROW, 1), *const init_reads = reads, *dev_reads;
  uint32_t       nreads = 0, *reads_len = (uint32_t *)calloc(MAX_READ_ROW, 4), *dev_reads_len;
  uint64_t       size   = (M / 8) * pow(4, K);

  cudaError_t     err_aln;
  struct timespec ts1, ts2;
  uint32_t i, j;

  //  cudaSetDevice(1);

  clock_gettime(CLOCK_REALTIME, &ts1);
  FILE  *fp;
  if((fp = fopen("pacbio_reads", "r")) == NULL) {
    perror("fopen");
    exit(EXIT_FAILURE);
  }

  while(fgets((char*)reads, MAX_READ_LEN, fp) != NULL) {
    reads_len[nreads++] = strlen((char *)reads);
    reads += MAX_READ_LEN;
  }
  fclose(fp);

  clock_gettime(CLOCK_REALTIME, &ts2);
  printf("read_time:%lf\n", ts2.tv_sec - ts1.tv_sec + (double)(ts2.tv_nsec - ts1.tv_nsec) / 1000000000.0);
  ts1 = ts2;

  cudaMalloc((void **)&dev_reads, MAX_READ_LEN * MEMCPYSIZE);
  cudaMalloc((void **)&dev_reads_len, nreads * 4);
  cudaMemcpy(dev_reads_len, reads_len, nreads * 4, cudaMemcpyHostToDevice);

  clock_gettime(CLOCK_REALTIME, &ts2);
  printf("malloc_time:%lf\n", ts2.tv_sec - ts1.tv_sec + (double)(ts2.tv_nsec - ts1.tv_nsec) / 1000000000.0);
  ts1 = ts2;
  if((err_aln = cudaGetLastError()) != cudaSuccess) {
    printf("Error! malloc:%s\n", cudaGetErrorString(err_aln));
    return 1;
  }

  for(i=0;i<nreads/MEMCPYSIZE;i++) {
    cudaMemcpy(dev_reads, init_reads + i * MAX_READ_LEN * MEMCPYSIZE, MAX_READ_LEN * MEMCPYSIZE, cudaMemcpyHostToDevice);
    kmer_aln<<<1, 1024>>>(dev_reads, dev_reads_len, i * MEMCPYSIZE, MEMCPYSIZE, QPOS, TPOS);
    cudaDeviceSynchronize();
    if((err_aln = cudaGetLastError()) != cudaSuccess) {
      printf("Error! kmer_aln:%s\n", cudaGetErrorString(err_aln));
      return 1;
    }
    clock_gettime(CLOCK_REALTIME, &ts2);
    printf("aln_time:%lf\n", ts2.tv_sec - ts1.tv_sec + (double)(ts2.tv_nsec - ts1.tv_nsec) / 1000000000.0);
    ts1 = ts2;

    return 0;


  }
  cudaMemcpy(dev_reads, init_reads + i * MAX_READ_LEN * MEMCPYSIZE, MAX_READ_LEN * (nreads % MEMCPYSIZE), cudaMemcpyHostToDevice);
  kmer_aln<<<1, 1024>>>(dev_reads, dev_reads_len, i * MEMCPYSIZE, (nreads % MEMCPYSIZE), QPOS, TPOS);
  cudaDeviceSynchronize();
  if((err_aln = cudaGetLastError()) != cudaSuccess) {
    printf("Error! kmer_aln:%s\n", cudaGetErrorString(err_aln));
    return 1;
  }
  clock_gettime(CLOCK_REALTIME, &ts2);
  printf("aln_time:%lf\n", ts2.tv_sec - ts1.tv_sec + (double)(ts2.tv_nsec - ts1.tv_nsec) / 1000000000.0);
  ts1 = ts2;


  return 0;
}
