#include <stdint.h>
#include <stdio.h>
extern "C" {

__device__ uint32_t warpReduceMin(uint32_t val) {
  uint32_t shfl;
  for(uint32_t offset = 16;offset > 0;offset >>= 1)
    if(val > (shfl = __shfl_xor(val, offset)))
      val = shfl;
  return val;
}

__device__ uint32_t warpReduceMax(uint32_t val) {
  uint32_t shfl;
  for(uint32_t offset = 16;offset > 0;offset >>= 1)
    if(val < (shfl = __shfl_xor(val, offset)))
      val = shfl;
  return val;
}

__inline__ __device__ uint32_t warpReduceSum(uint32_t val) {
  for(uint32_t offset = 16;offset > 0;offset >>= 1) 
    val += __shfl_xor(val, offset);
  return val;
}

__device__ uint32_t blockReduceMin(uint32_t val) {

  static __shared__ uint32_t shared[32];
  uint32_t lane = threadIdx.x & 0x1F;
  uint32_t wid = threadIdx.x >> 5;

  val = warpReduceMin(val);
  if(lane == 0)
    shared[wid] = val;
  __syncthreads();

  val = shared[lane];
  if(wid == 0) 
    val = warpReduceMin(val);
  __syncthreads();

  return val;
}

__device__ uint32_t blockReduceMax(uint32_t val) {

  static __shared__ uint32_t shared[32];
  uint32_t lane = threadIdx.x & 0x1F;
  uint32_t wid = threadIdx.x >> 5;

  val = warpReduceMax(val);
  if(lane == 0)
    shared[wid] = val;
  __syncthreads();

  val = shared[lane];
  if(wid == 0) 
    val = warpReduceMax(val);
  __syncthreads();

  return val;
}

__device__ uint32_t blockReduceSum(uint32_t val) {

  static __shared__ uint32_t shared[32];
  uint32_t lane = threadIdx.x & 0x1F;
  uint32_t wid = threadIdx.x >> 5;

  val = warpReduceSum(val);
  if(lane == 0)
    shared[wid] = val;
  __syncthreads();

  val = shared[lane];
  val = warpReduceSum(val);
  __syncthreads();

  return val;
}

__device__ uint32_t blockReduceMaxpos(uint32_t val, uint32_t pos) {

  static __shared__ uint32_t shared_val[32], shared_pos[32];

  uint32_t lane = threadIdx.x & 0x1F;
  uint32_t wid = threadIdx.x >> 5;

  for(uint32_t offset = 16;offset > 0;offset >>= 1) {
    uint32_t _val = __shfl_xor(val, offset);
    uint32_t _pos = __shfl_xor(pos, offset);
    if(_val > val) {
      val = _val;
      pos = _pos;
    }
  }

  if(lane == 0) {
    shared_val[wid] = val;
    shared_pos[wid] = pos;
  }
  __syncthreads();

  val = shared_val[lane];
  pos = shared_pos[lane];
                                                                                                                                                 
  for(uint32_t offset = 16;offset > 0;offset >>= 1) {
    uint32_t _val = __shfl_xor(val, offset);
    uint32_t _pos = __shfl_xor(pos, offset);
    if(_val > val) {
      val = _val;
      pos = _pos;
    }
  }
  __syncthreads();

  return pos;
}
}
